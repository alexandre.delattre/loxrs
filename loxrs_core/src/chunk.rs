use crate::chunk::OpCode::Constant;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use std::collections::HashMap;

use crate::value::{Value, ValueArray};

#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq, IntoPrimitive, TryFromPrimitive)]
pub enum OpCode {
    Constant,
    ConstantLong,
    Nil,
    True,
    False,
    Pop,
    GetLocal,
    SetLocal,
    GetGlobal,
    SetUpvalue,
    GetUpvalue,
    GetProperty,
    SetProperty,
    DefineGlobal,
    DefineGlobalVal,
    SetGlobal,
    Equal,
    Greater,
    Less,
    Add,
    Subtract,
    Multiply,
    Divide,
    Modulo,
    Not,
    Negate,
    Print,
    Jump,
    JumpIfFalse,
    Loop,
    Call,
    Closure,
    CloseUpvalue,
    Return,
    Class,
    Inherit,
    Method,
    Invoke,
    SuperInvoke,
}

type Line = u32;

pub struct Chunk {
    pub code: Vec<u8>,
    pub lines: Lines,
    pub constants: ValueArray,
    constant_cache: HashMap<Value, usize>,
}

impl Chunk {
    pub fn new() -> Self {
        Self {
            code: vec![],
            lines: Lines::new(),
            constants: ValueArray::new(),
            constant_cache: Default::default(),
        }
    }

    pub fn write(&mut self, byte: u8, line: Line) {
        self.code.push(byte);
        self.lines.push(line);
    }

    pub fn write_op(&mut self, code: OpCode, line: Line) {
        self.write(code as u8, line);
    }

    fn add_constant(&mut self, constant: Value) -> usize {
        if let Some(&offset) = self.constant_cache.get(&constant) {
            offset
        } else {
            self.constants.write(constant.clone());
            let offset = self.constants.values.len() - 1;
            self.constant_cache.insert(constant, offset);
            offset
        }
    }

    pub fn make_constant(&mut self, constant: Value) -> usize {
        let offset = self.add_constant(constant);
        if offset > u16::MAX as usize {
            panic!("max allowed constants {} reached", u16::MAX);
        }
        offset
    }

    pub fn write_constant(&mut self, constant: Value, line: Line) {
        let offset = self.add_constant(constant);
        match constant_offset(offset) {
            ConstantOffset::Small(o) => {
                self.write_op(Constant, line);
                self.write(o, line);
            }
            ConstantOffset::Long(o) => {
                self.write_op(OpCode::ConstantLong, line);
                for b in o.to_le_bytes() {
                    self.write(b, line);
                }
            }
        }
    }

    pub fn write_closure(&mut self, constant: Value, line: Line) {
        let offset = self.add_constant(constant);
        match constant_offset(offset) {
            ConstantOffset::Small(o) => {
                self.write_op(OpCode::Closure, line);
                self.write(o, line);
            }
            ConstantOffset::Long(_) => {
                panic!("max constant reached")
            }
        }
    }
}

enum ConstantOffset {
    Small(u8),
    Long(u16),
}

fn constant_offset(offset: usize) -> ConstantOffset {
    if offset <= u8::MAX as usize {
        ConstantOffset::Small(offset as u8)
    } else if offset <= u16::MAX as usize {
        ConstantOffset::Long(offset as u16)
    } else {
        panic!("max allowed constants {} reached", u16::MAX);
    }
}

pub struct Lines(Vec<(Line, u32)>);

impl Lines {
    pub fn new() -> Self {
        Lines(vec![])
    }

    pub fn push(&mut self, line: Line) {
        let last = self.0.last_mut();
        if let Some((l, n)) = last {
            if *l == line {
                *n += 1;
                return;
            }
        }
        self.0.push((line, 1));
    }

    pub fn get_line(&self, offset: usize) -> Line {
        let mut offset = offset + 1;
        let mut n = 0;
        loop {
            let (line, len) = self.0[n];
            let len = len as usize;
            if offset <= len {
                return line;
            }
            offset -= len;
            n += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::chunk::Lines;

    #[test]
    pub fn test_lines() {
        let mut lines = Lines::new();
        for _ in 0..10 {
            lines.push(1);
        }
        for _ in 0..10 {
            lines.push(2);
        }
        for _ in 0..10 {
            lines.push(3);
        }

        assert_eq!(lines.get_line(0), 1);
        assert_eq!(lines.get_line(1), 1);
        assert_eq!(lines.get_line(9), 1);

        assert_eq!(lines.get_line(10), 2);
        assert_eq!(lines.get_line(11), 2);
        assert_eq!(lines.get_line(19), 2);

        assert_eq!(lines.get_line(20), 3);
        assert_eq!(lines.get_line(21), 3);
        assert_eq!(lines.get_line(29), 3);
    }
}
