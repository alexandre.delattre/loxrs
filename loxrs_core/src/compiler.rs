use std::borrow::Cow;
use std::cell::{Cell, RefCell, RefMut};
use std::collections::HashMap;
use std::fmt::Write;
use std::ops::DerefMut;
use std::rc::Rc;
use std::sync::OnceLock;

use arrayvec::ArrayVec;
use num_enum::{IntoPrimitive, TryFromPrimitive};

use crate::chunk::{Chunk, OpCode};
#[cfg(feature = "debug_chunk")]
use crate::debug::disassemble_chunk;
use crate::object::{allocate_string, new_function, FunctionRef, ObjRef, SymbolTable};
use crate::scanner::{Scanner, Token, TokenType};
use crate::value::{RawNumber, Value};
use crate::writer::Writers;

pub struct Compiler<'a, 'b, 'c> {
    function: FunctionRef,
    fn_type: FunctionType,
    parser: Rc<RefCell<Parser<'a>>>,
    objects: &'b RefCell<Option<ObjRef>>,
    table: &'b RefCell<SymbolTable>,
    locals: RefCell<Vec<Local<'a>>>,
    upvalues: RefCell<ArrayVec<Upvalue, 256>>,
    enclosing: Option<&'c Compiler<'a, 'b, 'c>>,
    current_class: RefCell<Option<Rc<ClassCompiler>>>,
    scope_depth: Cell<i16>,
    writers: Writers,
}

struct ClassCompiler {
    has_superclass: Cell<bool>,
}

impl<'a, 'b, 'c> Compiler<'a, 'b, 'c> {
    pub fn new(
        fn_type: FunctionType,
        source: &'a str,
        objects: &'b RefCell<Option<ObjRef>>,
        table: &'b RefCell<SymbolTable>,
        writers: Writers,
    ) -> Self {
        let scanner = Scanner::new(source);
        let parser = Rc::new(RefCell::new(Parser::new(scanner, writers.clone())));

        let function = new_function(None);

        let compiler = Self {
            function,
            fn_type,
            parser,
            objects,
            table,
            locals: Default::default(),
            upvalues: Default::default(),
            scope_depth: Default::default(),
            enclosing: None,
            current_class: Default::default(),
            writers,
        };
        compiler.add_synth_local("");
        compiler
    }

    fn new_nested(
        fn_type: FunctionType,
        parser: Rc<RefCell<Parser<'a>>>,
        objects: &'b RefCell<Option<ObjRef>>,
        table: &'b RefCell<SymbolTable>,
        compiler: &'c Compiler<'a, 'b, 'c>,
    ) -> Self {
        let name = parser.borrow().get_previous().lexeme;
        let name_obj = allocate_string(Cow::Borrowed(name), &mut table.borrow_mut());
        let function = { new_function(Some(name_obj.as_obj_string())) };

        let compiler = Self {
            function,
            fn_type,
            parser,
            objects,
            table,
            locals: Default::default(),
            upvalues: Default::default(),
            scope_depth: Default::default(),
            enclosing: Some(compiler),
            current_class: compiler.current_class.clone(),
            writers: compiler.writers.clone(),
        };

        if fn_type != FunctionType::Function {
            compiler.add_synth_local("this");
        } else {
            compiler.add_synth_local("");
        }

        compiler
    }

    fn parser(&self) -> RefMut<Parser<'a>> {
        self.parser.borrow_mut()
    }

    pub fn compile(&self) -> Option<FunctionRef> {
        self.parser().advance();

        while !self.parser().matches(TokenType::Eof) {
            self.declaration();
        }

        let function = self.end_compiler();
        if !self.parser().had_error {
            Some(function)
        } else {
            None
        }
    }

    fn end_compiler(&self) -> FunctionRef {
        self.emit_return();

        #[cfg(feature = "debug_chunk")]
        {
            unsafe {
                let f = self.function.as_ref();
                let n = f.name.as_ref().map(|s| s.string.as_str()).unwrap_or("main");
                disassemble_chunk(
                    self.writers.trace_writer.borrow_mut().deref_mut(),
                    &*f.chunk.get(),
                    n,
                );
            }
        }
        self.function.clone()
    }

    fn begin_scope(&self) {
        self.scope_depth.set(self.scope_depth.get() + 1);
    }

    fn end_scope(&self) {
        self.scope_depth.set(self.scope_depth.get() - 1);

        let mut locals = self.locals.borrow_mut();
        while let Some(local) = locals.last() {
            if local.depth <= self.scope_depth.get() {
                break;
            }
            if local.is_captured {
                self.emit_op(OpCode::CloseUpvalue);
            } else {
                self.emit_op(OpCode::Pop);
            }
            locals.pop();
        }
    }
}

impl<'a> Compiler<'a, '_, '_> {
    fn declaration(&self) {
        if self.parser().matches(TokenType::Class) {
            self.class_declaration();
        } else if self.parser().matches(TokenType::Fun) {
            self.fun_declaration();
        } else if self.parser().matches(TokenType::Var) {
            self.var_declaration(true);
        } else if self.parser().matches(TokenType::Val) {
            self.var_declaration(false);
        } else {
            self.statement();
        }

        if self.parser().panic_mode {
            self.synchronize();
        }
    }

    fn class_declaration(&self) {
        self.parser()
            .consume(TokenType::Identifier, "Expect class name.");

        let class_name = self.parser().get_previous().clone();
        let name_constant = self.identifier_constant(class_name.clone());
        self.declare_variable();

        self.emit_op(OpCode::Class);
        self.emit_long_const(name_constant as u16);
        self.define_variable(name_constant, false);

        let enclosing = self.current_class.borrow().clone();

        let class_compiler = Rc::new(ClassCompiler {
            has_superclass: Cell::new(false),
        });

        *self.current_class.borrow_mut() = Some(class_compiler.clone());

        if self.parser().matches(TokenType::Less) {
            self.parser()
                .consume(TokenType::Identifier, "Expect superclass name.");
            self.variable(false);

            if class_name.lexeme == self.parser().get_previous().lexeme {
                self.parser()
                    .error_at_current("A class cannot inherit from itself.");
            }

            self.begin_scope();
            self.add_synth_local("super");
            self.define_variable(0, false);

            self.named_variable(class_name.clone(), false);
            self.emit_op(OpCode::Inherit);

            class_compiler.has_superclass.set(true);
        }

        self.named_variable(class_name, false);
        self.parser()
            .consume(TokenType::LeftBrace, "Expect '{' before class body.");

        while !self.parser().check(TokenType::RightBrace) && !self.parser().check(TokenType::Eof) {
            self.method();
        }

        self.parser()
            .consume(TokenType::RightBrace, "Expect '}' after class body.");
        self.emit_op(OpCode::Pop);

        if class_compiler.has_superclass.get() {
            self.end_scope();
        }

        *self.current_class.borrow_mut() = enclosing;
    }

    fn fun_declaration(&self) {
        let global = self.parse_variable("Expect function name.");
        self.mark_initialized(true);
        self.function(FunctionType::Function);
        self.define_variable(global, true);
    }

    fn var_declaration(&self, mutable: bool) {
        let global = self.parse_variable("Expect variable name.");

        if self.parser().matches(TokenType::Equal) {
            self.expression();
        } else {
            self.emit_op(OpCode::Nil)
        }
        self.parser().consume(
            TokenType::Semicolon,
            "Expect ';' after variable declaration.",
        );
        self.define_variable(global, mutable);
    }

    fn statement(&self) {
        if self.parser().matches(TokenType::Print) {
            self.print_statement();
        } else if self.parser().matches(TokenType::For) {
            self.for_statement();
        } else if self.parser().matches(TokenType::If) {
            self.if_statement();
        } else if self.parser().matches(TokenType::Return) {
            self.return_statement();
        } else if self.parser().matches(TokenType::While) {
            self.while_statement();
        } else if self.parser().matches(TokenType::Switch) {
            self.switch_statement();
        } else if self.parser().matches(TokenType::LeftBrace) {
            self.begin_scope();
            self.block();
            self.end_scope();
        } else {
            self.expression_statement()
        }
    }

    fn print_statement(&self) {
        self.expression();
        self.parser()
            .consume(TokenType::Semicolon, "Expect ';' after value.");
        self.emit_op(OpCode::Print);
    }

    fn synchronize(&self) {
        self.parser().panic_mode = false;

        while self.parser().get_current().kind != TokenType::Eof {
            if self.parser().get_previous().kind == TokenType::Semicolon {
                return;
            }

            match self.parser().get_current().kind {
                TokenType::Class
                | TokenType::Fun
                | TokenType::Var
                | TokenType::Val
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Switch
                | TokenType::Return => {
                    return;
                }
                _ => {}
            }

            self.parser().advance();
        }
    }

    fn expression_statement(&self) {
        self.expression();
        self.parser()
            .consume(TokenType::Semicolon, "Expect ';' after expression.");
        self.emit_op(OpCode::Pop);
    }

    fn if_statement(&self) {
        self.parser()
            .consume(TokenType::LeftParen, "Expect '(' after 'if'.");
        self.expression();
        self.parser()
            .consume(TokenType::RightParen, "Expect ')' after condition.");

        let then_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_op(OpCode::Pop);
        self.statement();

        let else_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(then_jump);
        self.emit_op(OpCode::Pop);

        if self.parser().matches(TokenType::Else) {
            self.statement();
        }

        self.patch_jump(else_jump);
    }

    fn return_statement(&self) {
        if self.fn_type == FunctionType::Script {
            self.parser()
                .error_at_current("Can't return from top-level code.");
        }

        if self.parser().matches(TokenType::Semicolon) {
            self.emit_return();
        } else {
            if self.fn_type == FunctionType::Initializer {
                self.parser()
                    .error_at_current("Can't return a value from an initializer.");
            }

            self.expression();
            self.parser()
                .consume(TokenType::Semicolon, "Expect ';' after return value.");
            self.emit_op(OpCode::Return);
        }
    }

    fn while_statement(&self) {
        let loop_start = self.current_chunk().code.len();
        self.parser()
            .consume(TokenType::LeftParen, "Expect '(' after 'while'.");
        self.expression();
        self.parser()
            .consume(TokenType::RightParen, "Expect ')' after condition.");

        let exit_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_op(OpCode::Pop);
        self.statement();
        self.emit_loop(loop_start);

        self.patch_jump(exit_jump);
        self.emit_op(OpCode::Pop);
    }

    fn for_statement(&self) {
        self.begin_scope();
        self.parser()
            .consume(TokenType::LeftParen, "Expect '(' after 'for'.");

        if self.parser().matches(TokenType::Semicolon) {
            // No initializer
        } else if self.parser().matches(TokenType::Var) {
            self.var_declaration(true);
        } else {
            self.expression_statement();
        }

        let mut loop_start = self.current_chunk().code.len();
        let mut exit_jump = None;
        if !self.parser().matches(TokenType::Semicolon) {
            self.expression();
            self.parser()
                .consume(TokenType::Semicolon, "Expect ';' after loop condition.");

            // Jump out of the loop if the condition is false
            exit_jump = Some(self.emit_jump(OpCode::JumpIfFalse));
            self.emit_op(OpCode::Pop);
        }

        if !self.parser().matches(TokenType::RightParen) {
            let body_jump = self.emit_jump(OpCode::Jump);
            let increment_start = self.current_chunk().code.len();
            self.expression();
            self.emit_op(OpCode::Pop);
            self.parser()
                .consume(TokenType::RightParen, "Expect ')' after for clauses.");

            self.emit_loop(loop_start);
            loop_start = increment_start;
            self.patch_jump(body_jump);
        }

        self.statement();
        self.emit_loop(loop_start);

        if let Some(exit_jump) = exit_jump {
            self.patch_jump(exit_jump);
            self.emit_op(OpCode::Pop);
        }

        self.end_scope();
    }

    fn switch_statement(&self) {
        self.begin_scope();

        self.parser()
            .consume(TokenType::LeftParen, "Expect '(' after 'switch'.");

        self.expression();

        let switch_var = self.add_synth_local("");

        self.parser()
            .consume(TokenType::RightParen, "Expect ')' after switch clause.");

        self.parser()
            .consume(TokenType::LeftBrace, "Expect '{' after switch.");

        let mut next_case_jump: Option<usize> = None;
        let mut exit_jumps: Vec<usize> = Vec::new();

        while !self.parser().check(TokenType::RightBrace) && !self.parser().check(TokenType::Eof) {
            if self.parser().matches(TokenType::Case) {
                if let Some(next_case_jump) = next_case_jump {
                    self.patch_jump(next_case_jump);
                    self.emit_op(OpCode::Pop);
                }

                self.expression();
                self.emit_op(OpCode::GetLocal);
                self.emit_byte(switch_var);

                self.emit_op(OpCode::Equal);

                next_case_jump = Some(self.emit_jump(OpCode::JumpIfFalse));
                self.emit_op(OpCode::Pop);

                self.parser()
                    .consume(TokenType::Colon, "Expect ':' after case expression.");

                self.begin_scope();
                while !self.parser().check(TokenType::Case)
                    && !self.parser().check(TokenType::Default)
                    && !self.parser().check(TokenType::RightBrace)
                    && !self.parser().check(TokenType::Eof)
                {
                    self.declaration();
                }
                self.end_scope();

                exit_jumps.push(self.emit_jump(OpCode::Jump));
            } else if self.parser().matches(TokenType::Default) {
                self.parser()
                    .consume(TokenType::Colon, "Expect ':' after case expression.");

                if let Some(next_case_jump) = next_case_jump {
                    self.patch_jump(next_case_jump);
                    self.emit_op(OpCode::Pop);
                }
                next_case_jump = None;

                self.begin_scope();
                while !self.parser().check(TokenType::RightBrace)
                    && !self.parser().check(TokenType::Eof)
                {
                    self.declaration();
                }
                self.end_scope();
            } else {
                self.parser()
                    .error_at_current("Expect 'case' or 'default' in switch body.");
                break;
            }
        }

        self.parser()
            .consume(TokenType::RightBrace, "Expect '}' after switch.");

        if let Some(next_case_jump) = next_case_jump {
            self.patch_jump(next_case_jump);
            self.emit_op(OpCode::Pop);
        }

        for exit in exit_jumps {
            self.patch_jump(exit);
        }

        self.end_scope();
    }

    fn expression(&self) {
        self.parse_precedence(Precedence::Assignment);
    }

    fn block(&self) {
        while !self.parser().check(TokenType::RightBrace) && !self.parser().check(TokenType::Eof) {
            self.declaration();
        }

        self.parser()
            .consume(TokenType::RightBrace, "Expect '}' after block.")
    }

    fn function(&self, fn_type: FunctionType) {
        let compiler =
            Compiler::new_nested(fn_type, self.parser.clone(), self.objects, self.table, self);
        compiler.begin_scope();

        compiler
            .parser()
            .consume(TokenType::LeftParen, "Expect '(' after 'function' name.");

        if !compiler.parser().check(TokenType::RightParen) {
            loop {
                {
                    let f = compiler.function.as_ref();
                    f.inc_arity();
                    if f.arity.get() > 255 {
                        compiler
                            .parser()
                            .error_at_current("Can't have more than 255 parameters.");
                    }
                }
                let constant = compiler.parse_variable("Expect parameter name.");
                compiler.define_variable(constant, false);

                if !compiler.parser().matches(TokenType::Comma) {
                    break;
                }
            }
        }

        compiler
            .parser()
            .consume(TokenType::RightParen, "Expect ')' after parameters.");
        compiler
            .parser()
            .consume(TokenType::LeftBrace, "Expect '{' before function body");

        compiler.block();
        let f = compiler.end_compiler();

        {
            let p = self.parser();
            let prev = p.get_previous();
            self.current_chunk()
                .write_closure(Value::Object(f.clone()), prev.line);
        }

        let upvalues = compiler.upvalues.borrow();
        f.as_ref().upvalue_count.set(upvalues.len() as u8);

        for upvalue in upvalues.iter() {
            self.emit_byte(if upvalue.is_local { 1 } else { 0 });
            self.emit_byte(upvalue.index);
        }
    }

    fn method(&self) {
        self.parser()
            .consume(TokenType::Identifier, "Expect method name.");
        let previous_token = self.parser().get_previous().clone();
        let constant = self.identifier_constant(previous_token.clone());

        let fn_type = if previous_token.lexeme == "init" {
            FunctionType::Initializer
        } else {
            FunctionType::Method
        };

        self.function(fn_type);
        self.emit_op(OpCode::Method);
        self.emit_long_const(constant as u16);
    }

    fn grouping(&self, _can_assign: bool) {
        self.expression();
        self.parser()
            .consume(TokenType::RightParen, "Expect ')' after expression.");
    }

    fn number(&self, _can_assign: bool) {
        let p = self.parser();
        let prev = p.get_previous();
        let value: RawNumber = prev.lexeme.parse().unwrap();
        self.current_chunk()
            .write_constant(Value::Number(value), prev.line);
    }

    fn string(&self, _can_assign: bool) {
        let prev = self.parser().get_previous().clone();
        let lexeme = prev.lexeme;
        let s = &lexeme[1..lexeme.len() - 1];

        let obj = Value::Object(allocate_string(
            Cow::Borrowed(s),
            &mut self.table.borrow_mut(),
        ));
        self.current_chunk().write_constant(obj, prev.line)
    }

    fn variable(&self, can_assign: bool) {
        let token = self.parser().get_previous().clone();
        self.named_variable(token, can_assign)
    }

    fn this(&self, _can_assign: bool) {
        if self.current_class.borrow().is_none() {
            self.parser()
                .error_at_current("Cannot use 'this' outside of a class.");
            return;
        }

        self.variable(false);
    }

    fn super_(&self, _can_assign: bool) {
        let Some(class) = self.current_class.borrow().clone() else {
            return self
                .parser()
                .error_at_current("Cannot use 'super' outside of a class.");
        };
        if !class.has_superclass.get() {
            return self
                .parser()
                .error_at_current("Cannot use 'super' in a class with no superclass.");
        }

        self.parser()
            .consume(TokenType::Dot, "Expect '.' after 'super'.");
        self.parser()
            .consume(TokenType::Identifier, "Expect superclass method name.");
        let name = self.identifier_constant(self.parser().get_previous().clone());

        self.named_variable(Token::synthetic("this"), false);

        self.parser().consume(
            TokenType::LeftParen,
            "Expect '(' after superclass method name.",
        );
        let arg_count = self.argument_list();
        self.named_variable(Token::synthetic("super"), false);
        self.emit_op(OpCode::SuperInvoke);
        self.emit_long_const(name as u16);
        self.emit_byte(arg_count);
    }

    fn named_variable(&self, name: Token<'a>, can_assign: bool) {
        if let Some((arg, mutable)) = self.resolve_local(name.clone()) {
            if can_assign && self.parser().matches(TokenType::Equal) {
                if !mutable {
                    self.parser().error_at_current(&format!(
                        "Cannot assign to local value '{}'",
                        name.lexeme
                    ));
                }
                self.expression();
                self.emit_op(OpCode::SetLocal);
                self.emit_byte(arg);
            } else {
                self.emit_op(OpCode::GetLocal);
                self.emit_byte(arg);
            }
        } else if let Some((arg, mutable)) = self.resolve_upvalue(name.clone()) {
            if can_assign && self.parser().matches(TokenType::Equal) {
                if !mutable {
                    self.parser()
                        .error_at_current(&format!("Cannot assign to upvalue '{}'", name.lexeme));
                }
                self.expression();
                self.emit_op(OpCode::SetUpvalue);
                self.emit_byte(arg);
            } else {
                self.emit_op(OpCode::GetUpvalue);
                self.emit_byte(arg);
            }
        } else {
            let arg = self.identifier_constant(name);

            if can_assign && self.parser().matches(TokenType::Equal) {
                self.expression();
                self.emit_op(OpCode::SetGlobal);
                self.emit_long_const(arg as u16);
            } else {
                self.emit_op(OpCode::GetGlobal);
                self.emit_long_const(arg as u16);
            }
        }
    }

    fn unary(&self, _can_assign: bool) {
        let prev = self.parser().get_previous().clone();
        let operator_type = prev.kind;

        self.parse_precedence(Precedence::Unary);
        match operator_type {
            TokenType::Bang => self.emit_op(OpCode::Not),
            TokenType::Minus => self.emit_op(OpCode::Negate),
            _ => panic!("unreachable"),
        }
    }

    fn binary(&self, _can_assign: bool) {
        let prev = self.parser().get_previous().clone();
        let operator_type = prev.kind;

        let rule = get_rule(operator_type);
        self.parse_precedence(rule.precedence.next());

        match operator_type {
            TokenType::BangEqual => self.emit_ops(&[OpCode::Equal, OpCode::Not]),
            TokenType::EqualEqual => self.emit_op(OpCode::Equal),

            TokenType::Greater => self.emit_op(OpCode::Greater),
            TokenType::GreaterEqual => self.emit_ops(&[OpCode::Less, OpCode::Not]),

            TokenType::Less => self.emit_op(OpCode::Less),
            TokenType::LessEqual => self.emit_ops(&[OpCode::Greater, OpCode::Not]),

            TokenType::Plus => self.emit_op(OpCode::Add),
            TokenType::Minus => self.emit_op(OpCode::Subtract),
            TokenType::Star => self.emit_op(OpCode::Multiply),
            TokenType::Slash => self.emit_op(OpCode::Divide),
            TokenType::Percent => self.emit_op(OpCode::Modulo),
            _ => panic!("unreachable"),
        }
    }

    fn call(&self, _can_assign: bool) {
        let arg_count = self.argument_list();
        self.emit_op(OpCode::Call);
        self.emit_byte(arg_count);
    }

    fn dot(&self, can_assign: bool) {
        self.parser()
            .consume(TokenType::Identifier, "Expected property name after '.'.");
        let name = self.identifier_constant(self.parser().get_previous().clone()) as u16;

        if can_assign && self.parser().matches(TokenType::Equal) {
            self.expression();
            self.emit_op(OpCode::SetProperty);
            self.emit_long_const(name);
        } else if self.parser().matches(TokenType::LeftParen) {
            let arg_count = self.argument_list();
            self.emit_op(OpCode::Invoke);
            self.emit_long_const(name);
            self.emit_byte(arg_count);
        } else {
            self.emit_op(OpCode::GetProperty);
            self.emit_long_const(name);
        }
    }

    fn and(&self, _can_assign: bool) {
        let end_jump = self.emit_jump(OpCode::JumpIfFalse);

        self.emit_op(OpCode::Pop);
        self.parse_precedence(Precedence::And);

        self.patch_jump(end_jump);
    }

    fn or(&self, _can_assign: bool) {
        let else_jump = self.emit_jump(OpCode::JumpIfFalse);
        let end_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(else_jump);
        self.emit_op(OpCode::Pop);

        self.parse_precedence(Precedence::Or);
        self.patch_jump(end_jump);
    }

    fn literal(&self, _can_assign: bool) {
        let token_type = self.parser().get_previous().kind;

        match token_type {
            TokenType::False => self.emit_op(OpCode::False),
            TokenType::Nil => self.emit_op(OpCode::Nil),
            TokenType::True => self.emit_op(OpCode::True),
            _ => {}
        }
    }

    fn parse_precedence(&self, precedence: Precedence) {
        self.parser().advance();
        let Some(prefix_rule) = get_rule(self.parser().get_previous().kind).prefix else {
            self.parser().error_at_current("Expect expression.");
            return;
        };

        let can_assign = precedence <= Precedence::Assignment;
        prefix_rule(self, can_assign);

        while precedence <= get_rule(self.parser().get_current().kind).precedence {
            self.parser().advance();
            let infix_rule = get_rule(self.parser().get_previous().kind).infix.unwrap();
            infix_rule(self, can_assign);
        }

        if can_assign && self.parser().matches(TokenType::Equal) {
            self.parser().error_at_current("Invalid assignment target.");
        }
    }

    fn parse_variable(&self, message: &str) -> usize {
        self.parser().consume(TokenType::Identifier, message);

        self.declare_variable();
        if self.scope_depth.get() > 0 {
            return 0;
        }
        let token = self.parser().get_previous().clone();
        self.identifier_constant(token)
    }

    fn identifier_constant(&self, token: Token<'a>) -> usize {
        let value = Value::Object(allocate_string(
            Cow::Borrowed(token.lexeme),
            &mut self.table.borrow_mut(),
        ));
        self.current_chunk().make_constant(value)
    }

    fn declare_variable(&self) {
        if self.scope_depth.get() == 0 {
            return;
        }
        let name = self.parser().get_previous().clone();
        let locals = self.locals.borrow();

        for i in (0..locals.len()).rev() {
            let local = &locals[i];
            if local.depth != -1 && local.depth < self.scope_depth.get() {
                break;
            }

            if name.lexeme == local.name.lexeme {
                self.parser()
                    .error_at_current("Already a variable with this name in this scope");
            }
        }
        drop(locals);

        self.add_local(name);
    }

    fn add_local(&self, name: Token<'a>) {
        let mut locals = self.locals.borrow_mut();
        if locals.len() == u8::MAX as usize {
            self.parser()
                .error_at_current("Too many local variables in function.");
            return;
        }
        let local = Local::new_uninitialized(name);
        locals.push(local);
    }

    fn add_synth_local(&self, name: &'static str) -> u8 {
        let mut locals = self.locals.borrow_mut();
        if locals.len() == u8::MAX as usize {
            self.parser()
                .error_at_current("Too many local variables in function.");
            return 0;
        }
        let local = Local::new_synthetic(name, self.scope_depth.get());
        locals.push(local);
        (locals.len() - 1) as u8
    }

    fn define_variable(&self, global: usize, mutable: bool) {
        if self.scope_depth.get() > 0 {
            self.mark_initialized(mutable);
            return;
        }

        self.emit_op(if mutable {
            OpCode::DefineGlobal
        } else {
            OpCode::DefineGlobalVal
        });
        let [b0, b1] = (global as u16).to_le_bytes();
        self.emit_bytes(b0, b1);
    }

    fn argument_list(&self) -> u8 {
        let mut arg_count = 0;

        if !self.parser().check(TokenType::RightParen) {
            loop {
                self.expression();
                if arg_count == 255 {
                    self.parser()
                        .error_at_current("Can't have more than 255 arguments.");
                }
                arg_count += 1;
                if !self.parser().matches(TokenType::Comma) {
                    break;
                }
            }
        }

        self.parser()
            .consume(TokenType::RightParen, "Expect ')' after arguments.");

        arg_count
    }

    fn resolve_local(&self, name: Token<'a>) -> Option<(u8, bool)> {
        let locals = self.locals.borrow();
        for i in (0..locals.len()).rev() {
            let local = &locals[i];
            if name.lexeme == local.name.lexeme {
                if local.depth == -1 {
                    self.parser()
                        .error_at_current("Can't read local variable in its own initializer");
                }
                return Some((i as u8, local.mutable));
            }
        }
        None
    }

    fn resolve_upvalue(&self, name: Token<'a>) -> Option<(u8, bool)> {
        let enclosing = self.enclosing?;

        if let Some((local, mutable)) = enclosing.resolve_local(name.clone()) {
            enclosing.locals.borrow_mut()[local as usize].is_captured = true;
            return Some((self.add_upvalue(local, true), mutable));
        };

        if let Some((upvalue, mutable)) = enclosing.resolve_upvalue(name) {
            return Some((self.add_upvalue(upvalue, false), mutable));
        }

        None
    }

    fn add_upvalue(&self, index: u8, is_local: bool) -> u8 {
        let mut upvalues = self.upvalues.borrow_mut();

        for (i, u) in upvalues.iter().enumerate() {
            if u.index == index && u.is_local == is_local {
                return i as u8;
            }
        }
        let upvalue = Upvalue::new(index, is_local);
        let up_index = upvalues.len() as u8;
        upvalues.push(upvalue); // TODO error management ?
        up_index
    }

    fn mark_initialized(&self, mutable: bool) {
        let mut locals = self.locals.borrow_mut();
        if self.scope_depth.get() == 0 {
            return;
        }
        let loc_mut = locals.last_mut().unwrap();
        loc_mut.depth = self.scope_depth.get();
        loc_mut.mutable = mutable;
    }
}

impl Compiler<'_, '_, '_> {
    #[allow(clippy::mut_from_ref)] // TODO check soundness
    fn current_chunk(&self) -> &mut Chunk {
        unsafe { &mut *self.function.as_ref().chunk.get() }
    }

    fn emit_byte(&self, byte: u8) {
        self.current_chunk()
            .write(byte, self.parser().get_previous().line)
    }

    fn emit_op(&self, op: OpCode) {
        self.current_chunk()
            .write_op(op, self.parser().get_previous().line)
    }

    fn emit_return(&self) {
        if self.fn_type == FunctionType::Initializer {
            self.emit_op(OpCode::GetLocal);
            self.emit_byte(0);
        } else {
            self.emit_op(OpCode::Nil);
        }
        self.emit_op(OpCode::Return);
    }

    fn emit_bytes(&self, byte1: u8, byte2: u8) {
        let l = self.parser().get_previous().line;
        let chunk = self.current_chunk();
        chunk.write(byte1, l);
        chunk.write(byte2, l);
    }

    fn emit_ops(&self, ops: &[OpCode]) {
        let l = self.parser().get_previous().line;
        let chunk = self.current_chunk();
        for op in ops {
            chunk.write_op(*op, l);
        }
    }

    fn emit_long_const(&self, constant: u16) {
        let [b0, b1] = constant.to_le_bytes();
        self.emit_bytes(b0, b1);
    }

    fn emit_jump(&self, instruction: OpCode) -> usize {
        let l = self.parser().get_previous().line;
        let chunk = self.current_chunk();
        chunk.write_op(instruction, l);
        chunk.write(0xff, l);
        chunk.write(0xff, l);
        chunk.code.len() - 2
    }

    fn emit_loop(&self, loop_start: usize) {
        self.emit_op(OpCode::Loop);
        let offset = self.current_chunk().code.len() - loop_start + 2;
        if offset > u16::MAX as usize {
            self.parser().error_at_current("Loop body too large");
        }
        let offset = offset as u16;
        let [b0, b1] = offset.to_le_bytes();
        self.emit_bytes(b0, b1);
    }

    fn patch_jump(&self, offset: usize) {
        let jump = self.current_chunk().code.len() - offset - 2;
        if jump > u16::MAX as usize {
            self.parser()
                .error_at_current("Too much code to jump over.");
        }
        let jump = jump as u16;
        let [b0, b1] = jump.to_le_bytes();
        self.current_chunk().code[offset] = b0;
        self.current_chunk().code[offset + 1] = b1;
    }
}

struct Parser<'a> {
    scanner: Scanner<'a>,
    current: Option<Token<'a>>,
    previous: Option<Token<'a>>,
    had_error: bool,
    panic_mode: bool,
    writers: Writers,
}

impl<'a> Parser<'a> {
    fn new(scanner: Scanner<'a>, writers: Writers) -> Self {
        Self {
            scanner,
            current: None,
            previous: None,
            had_error: false,
            panic_mode: false,
            writers,
        }
    }

    fn advance(&mut self) {
        self.previous.clone_from(&self.current);

        loop {
            self.current = Some(self.scanner.scan_token());

            if self.get_current().kind != TokenType::Error {
                break;
            }

            let msg = self.get_current().lexeme;
            self.error_at_current(msg);
        }
    }

    fn consume(&mut self, kind: TokenType, message: &str) {
        if self.get_current().kind == kind {
            self.advance();
            return;
        }

        self.error_at_current(message);
    }

    fn matches(&mut self, token_type: TokenType) -> bool {
        if !self.check(token_type) {
            return false;
        }
        self.advance();
        true
    }

    fn check(&self, token_type: TokenType) -> bool {
        self.get_current().kind == token_type
    }

    fn get_current(&self) -> &Token<'a> {
        self.current.as_ref().unwrap()
    }

    fn get_previous(&self) -> &Token<'a> {
        self.previous.as_ref().unwrap()
    }
}

impl<'a> Parser<'a> {
    fn error_at_current(&mut self, message: &str) {
        let token = self.get_current().clone();
        self.error_at(&token, message);
    }

    fn error_at(&mut self, token: &Token<'a>, message: &str) {
        if self.panic_mode {
            return;
        }
        self.panic_mode = true;
        let mut writer = self.writers.err_writer.borrow_mut();
        write!(writer, "[line {}] Error", token.line).unwrap();

        match token.kind {
            TokenType::Eof => write!(writer, " at end").unwrap(),
            TokenType::Error => {}
            _ => write!(writer, " at {}", token.lexeme).unwrap(),
        }
        writeln!(writer, ": {message}").unwrap();
        self.had_error = true;
    }
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, IntoPrimitive, TryFromPrimitive, Ord, PartialOrd, Eq, PartialEq)]
enum Precedence {
    None,
    Assignment, // =
    Or,         // or
    And,        // and
    Equality,   // == !=
    Comparison, // < > <= >=
    Term,       // + -
    Factor,     // * /
    Unary,      // ! -
    Call,       // . ()
    Primary,
}

impl Precedence {
    fn next(self) -> Precedence {
        ((self as u8) + 1).try_into().unwrap()
    }
}

type ParseFn = for<'a, 'b, 'c> fn(&Compiler<'a, 'b, 'c>, can_assign: bool);

struct ParseRule {
    prefix: Option<ParseFn>,
    infix: Option<ParseFn>,
    precedence: Precedence,
}

fn get_rule(token_type: TokenType) -> &'static ParseRule {
    rules().get(&token_type).unwrap_or(&DEFAULT_RULE)
}

fn rules() -> &'static HashMap<TokenType, ParseRule> {
    static HASHMAP: OnceLock<HashMap<TokenType, ParseRule>> = OnceLock::new();
    HASHMAP.get_or_init(build_rules)
}

macro_rules! _parse_fn {
    (None) => {
        None
    };
    ($fn: ident) => {
        Some(|c, can_assign| c.$fn(can_assign))
    };
}

macro_rules! _rule {
    (
        ($prefix: tt, $infix: tt, $precedence: ident)
    ) => {
        ParseRule {
            prefix: _parse_fn!($prefix),
            infix: _parse_fn!($infix),
            precedence: Precedence::$precedence,
        }
    };
}

macro_rules! _rules {
    ($($token: ident => $rule: tt)*) => {
        let mut rules = HashMap::<TokenType, ParseRule>::new();

        $(
            rules.insert(
                TokenType::$token,
                _rule!($rule)
            );
        )*

        rules
    };
}

static DEFAULT_RULE: ParseRule = _rule!((None, None, None));

fn build_rules() -> HashMap<TokenType, ParseRule> {
    _rules! {
        LeftParen => (grouping, call, Call)

        Number => (number, None, None)
        Nil => (literal, None, None)
        This => (this, None, None)
        Super => (super_, None, None)
        True => (literal, None, None)
        False => (literal, None, None)
        Identifier => (variable, None, None)
        String => (string, None, None)

        BangEqual => (None, binary, Equality)
        EqualEqual => (None, binary, Equality)

        Greater => (None, binary, Comparison)
        GreaterEqual => (None, binary, Comparison)
        Less => (None, binary, Comparison)
        LessEqual => (None, binary, Comparison)

        Bang => (unary, None, None)
        Minus => (unary, binary, Term)
        Plus => (None, binary, Term)
        Star => (None, binary, Factor)
        Slash => (None, binary, Factor)
        Percent => (None, binary, Factor)

        And => (None, and, And)
        Or => (None, or, Or)

        Dot => (None, dot, Call)
    }
}

struct Local<'a> {
    name: Token<'a>,
    depth: i16,
    mutable: bool,
    is_captured: bool,
}

impl<'a> Local<'a> {
    pub fn new_uninitialized(name: Token<'a>) -> Self {
        Self {
            name,
            depth: -1,
            mutable: false,
            is_captured: false,
        }
    }

    pub fn new_synthetic(name: &'static str, depth: i16) -> Self {
        Self {
            name: Token::synthetic(name),
            depth,
            mutable: false,
            is_captured: false,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum FunctionType {
    Script,
    Function,
    Initializer,
    Method,
}

#[derive(Debug)]
struct Upvalue {
    index: u8,
    is_local: bool,
}

impl Upvalue {
    pub fn new(index: u8, is_local: bool) -> Self {
        Self { index, is_local }
    }
}
