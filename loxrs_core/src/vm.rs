use crate::chunk::{Chunk, OpCode};
use crate::compiler::{Compiler, FunctionType};
#[cfg(feature = "debug_trace")]
use crate::debug::disassemble_instruction;
use crate::debug::print_value;
use crate::object::{
    allocate_string, new_bound_method, new_class, new_closure, new_instance, new_native,
    new_upvalue, ClassRef, ClosureRef, NativeFn, ObjRef, ObjectMatchResult, StringRef, SymbolTable,
    Table, UpvalueRef, UpvalueState,
};
use crate::value::Value;
use crate::writer::Writers;
use arrayvec::ArrayVec;
use core::fmt;
use std::borrow::Cow;
use std::cell::{Cell, OnceCell, RefCell};
use std::collections::hash_map::Entry;
use std::error::Error;
use std::fmt::{Display, Formatter, Write};
use std::ops::{Deref, DerefMut};
use std::time::SystemTime;

pub struct Vm {
    stack: ArrayVec<Value, 256>,
    frames: ArrayVec<CallFrame, 64>,
    globals: GlobalMap,
    objects: Box<RefCell<Option<ObjRef>>>,
    strings: Box<RefCell<SymbolTable>>,
    open_upvalues: Box<RefCell<Option<UpvalueRef>>>,
    init_string: OnceCell<StringRef>,
    writers: Writers,
}

impl Vm {
    pub fn new(writers: Writers) -> Self {
        let mut vm = Vm {
            stack: ArrayVec::default(),
            frames: ArrayVec::default(),
            globals: GlobalMap::default(),
            objects: Box::new(RefCell::new(None)),
            strings: Box::new(RefCell::new(SymbolTable::default())),
            open_upvalues: Box::new(RefCell::new(None)),
            init_string: OnceCell::new(),
            writers,
        };

        vm.define_native("clock", clock_native, 0);
        vm
    }

    pub fn interpret_source(&mut self, source: &str) -> Result<(), InterpretError> {
        let compiler = Compiler::new(
            FunctionType::Script,
            source,
            &self.objects,
            &self.strings,
            self.writers.clone(),
        );
        if let Some(function) = compiler.compile() {
            self.push(Value::Object(function.clone()));
            let closure = { new_closure(function) };
            self.pop();
            self.push(Value::Object(closure.clone()));
            let mut ip = 0;
            self.call(closure, 0, &mut ip)?;
            self.run()
        } else {
            Err(InterpretError::CompileError)
        }
    }
}

macro_rules! binop {
    ($self: ident, $type: expr, $op: tt, $ip: expr) => {
        if !$self.check_number(0) || !$self.check_number(1) {
            return Err($self.runtime_error("Operands must be numbers.", $ip));
        }

        let b = $self.pop().unwrap_number();
        let a = $self.pop().unwrap_number();
        $self.push($type(a $op b));
    };
}

impl Vm {
    pub fn run(&mut self) -> Result<(), InterpretError> {
        let mut ip = 0usize;

        loop {
            let frame = self.curr_frame();
            let chunk = frame.borrow_chunk();

            #[cfg(feature = "debug_trace")]
            {
                let mut w = self.writers.trace_writer.borrow_mut();
                write!(w, "        ").unwrap();
                for value in &self.stack {
                    write!(w, "[ ").unwrap();
                    print_value(w.deref_mut(), value);
                    write!(w, " ]").unwrap();
                }
                writeln!(w).unwrap();
                disassemble_instruction(w.deref_mut(), chunk, ip);
            }

            let instruction = read_byte(chunk, &mut ip);
            let instruction = OpCode::try_from(instruction).expect("invalid opcode encountered");

            //let instruction = unsafe { std::mem::transmute::<u8, OpCode>(instruction) };
            match instruction {
                OpCode::Constant => {
                    let constant = read_constant(chunk, &mut ip);
                    self.push(constant);
                }
                OpCode::ConstantLong => {
                    let constant = read_long_constant(chunk, &mut ip);
                    self.push(constant);
                }
                OpCode::Nil => {
                    self.push(Value::Nil);
                }
                OpCode::True => {
                    self.push(Value::Bool(true));
                }
                OpCode::False => {
                    self.push(Value::Bool(false));
                }
                OpCode::Pop => {
                    self.pop();
                }
                OpCode::GetLocal => {
                    let frame = self.curr_frame();
                    let slot = read_byte(chunk, &mut ip) as usize + frame.start;
                    self.push(self.stack[slot].clone());
                }
                OpCode::SetLocal => {
                    let frame = self.curr_frame();
                    let slot = read_byte(chunk, &mut ip) as usize + frame.start;
                    self.stack[slot] = self.peek(0).unwrap().clone();
                }
                OpCode::GetGlobal => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    if let Some(value) = self.globals.get(name.clone()) {
                        self.push(value.value.clone());
                    } else {
                        let name_str = &name.string;
                        self.runtime_error(&format!("Undefined variable '{name_str}'."), ip);
                        return Err(InterpretError::RuntimeError);
                    }
                }
                OpCode::DefineGlobal => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let value = self.peek(0).unwrap().clone();
                    // println!("Setting {:?} to {value:?}", &name);
                    self.globals.insert(name, Global::new(value, true));
                    self.pop();
                }
                OpCode::DefineGlobalVal => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let value = self.peek(0).unwrap().clone();
                    self.globals.insert(name, Global::new(value, false));
                    self.pop();
                }
                OpCode::SetGlobal => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let value = self.peek(0).unwrap().clone();
                    let e = self.globals.entry(name.clone());
                    match e {
                        Entry::Occupied(mut occ) => {
                            let global = occ.get_mut();
                            if global.mutable {
                                global.value = value;
                            } else {
                                let name_str = &name.string;
                                self.runtime_error(
                                    &format!("Cannot redefine global const '{name_str}'."),
                                    ip,
                                );
                                return Err(InterpretError::RuntimeError);
                            }
                        }
                        Entry::Vacant(_) => {
                            let name_str = &name.string;
                            self.runtime_error(&format!("Undefined variable '{name_str}'."), ip);
                            return Err(InterpretError::RuntimeError);
                        }
                    }
                }
                OpCode::GetUpvalue => {
                    let slot = read_byte(chunk, &mut ip) as usize;
                    let upvalue = frame.closure.upvalues.borrow()[slot].clone();

                    if upvalue.is_open() {
                        self.push(self.stack[upvalue.slot].clone());
                    } else {
                        self.push(upvalue.get_closed_value());
                    }
                }
                OpCode::SetUpvalue => {
                    let slot = read_byte(chunk, &mut ip) as usize;
                    let upvalue = frame.closure.upvalues.borrow()[slot].clone();

                    if upvalue.is_open() {
                        self.stack[upvalue.slot] = self.peek(0).unwrap().clone();
                    } else {
                        upvalue
                            .state
                            .replace(UpvalueState::Closed(self.peek(0).unwrap().clone()));
                    }
                }
                OpCode::GetProperty => {
                    let Ok(instance) = self.peek(0).unwrap().try_instance() else {
                        return Err(self.runtime_error("Only instances have properties.", ip));
                    };
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let maybe_field = instance.fields.borrow().get(name.clone()).cloned();
                    if let Some(value) = maybe_field {
                        self.pop();
                        self.push(value);
                    } else {
                        self.bind_method(instance.class.clone(), name.clone())?;
                    }
                }
                OpCode::SetProperty => {
                    let Ok(instance) = self.peek(1).unwrap().try_instance() else {
                        return Err(self.runtime_error("Only instances have properties.", ip));
                    };

                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let value = self.pop();

                    instance.fields.borrow_mut().insert(name, value.clone());
                    self.pop();
                    self.push(value);
                }
                OpCode::Equal => {
                    let b = self.pop();
                    let a = self.pop();
                    self.push(Value::Bool(a == b))
                }
                OpCode::Greater => {
                    binop!(self, Value::Bool, >, ip);
                }
                OpCode::Less => {
                    binop!(self, Value::Bool, <, ip);
                }
                OpCode::Add => {
                    if self.check_string(1) {
                        self.concatenate();
                    } else if self.check_number(0) && self.check_number(1) {
                        let b = self.pop().unwrap_number();
                        let a = self.pop().unwrap_number();
                        self.push(Value::Number(a + b));
                    } else {
                        return Err(
                            self.runtime_error("Operands must be two numbers or two strings.", ip)
                        );
                    }
                }
                OpCode::Subtract => {
                    binop!(self, Value::Number, -, ip);
                }
                OpCode::Multiply => {
                    binop!(self, Value::Number, *, ip);
                }
                OpCode::Divide => {
                    binop!(self, Value::Number, /, ip);
                }
                OpCode::Modulo => {
                    binop!(self, Value::Number, %, ip);
                }
                OpCode::Not => {
                    let val = self.pop();
                    self.push(Value::Bool(is_falsey(&val)))
                }
                OpCode::Negate => {
                    if !self.check_number(0) {
                        return Err(self.runtime_error("Operands must be numbers.", ip));
                    }

                    let val = self.pop();
                    self.push(Value::Number(-val.unwrap_number()));
                }
                OpCode::Print => {
                    let val = self.pop();
                    let mut writer = self.writers.writer.borrow_mut();
                    print_value(writer.deref_mut(), &val);
                    writeln!(writer).unwrap();
                }
                OpCode::Jump => {
                    let offset = read_short(chunk, &mut ip) as usize;
                    ip += offset;
                }
                OpCode::Loop => {
                    let offset = read_short(chunk, &mut ip) as usize;
                    ip -= offset;
                }
                OpCode::JumpIfFalse => {
                    let offset = read_short(chunk, &mut ip) as usize;
                    if is_falsey(self.peek(0).unwrap()) {
                        ip += offset;
                    }
                }
                OpCode::Call => {
                    let arg_count = read_byte(chunk, &mut ip);
                    // Save current ip to frame
                    self.curr_frame().ip.set(ip);
                    let v = self.peek(arg_count as usize).unwrap().clone();
                    self.call_value(v, arg_count, &mut ip)?;
                }
                OpCode::Invoke => {
                    let method = read_long_constant(chunk, &mut ip).unwrap_string();
                    let arg_count = read_byte(chunk, &mut ip);
                    // Save current ip to frame
                    self.curr_frame().ip.set(ip);
                    self.invoke(method, arg_count, &mut ip)?;
                }
                OpCode::SuperInvoke => {
                    let method = read_long_constant(chunk, &mut ip).unwrap_string();
                    let arg_count = read_byte(chunk, &mut ip);
                    let super_class = self.pop().unwrap_class();

                    // Save current ip to frame
                    self.curr_frame().ip.set(ip);
                    self.invoke_from_class(super_class, method, arg_count, &mut ip)?;
                }
                OpCode::Closure => {
                    let f_val = read_constant(chunk, &mut ip);
                    let f = f_val.unwrap_function();
                    let closure = new_closure(f.clone());

                    for _ in 0..f.upvalue_count.get() {
                        let is_local = read_byte(chunk, &mut ip) == 1;
                        let index = read_byte(chunk, &mut ip) as usize;
                        if is_local {
                            closure.as_ref().upvalues.borrow_mut().push(capture_upvalue(
                                frame.start + index,
                                &mut self.open_upvalues.borrow_mut(),
                            ))
                        } else {
                            (*closure.as_ref().upvalues.borrow_mut())
                                .push(frame.closure.as_ref().upvalues.borrow()[index].clone())
                        }
                    }

                    self.push(Value::Object(closure));
                }
                OpCode::CloseUpvalue => {
                    self.close_upvalues(self.stack.len() - 1);
                    self.pop();
                }
                OpCode::Return => {
                    let res = self.pop();
                    let frame = self.frames.pop().unwrap();
                    self.close_upvalues(frame.start);
                    if self.frames.is_empty() {
                        self.pop();
                        return Ok(());
                    }

                    // self.stack.resize(frame.start, Value::Nil);
                    // TODO find a more efficient equivalent to the line above
                    while self.stack.len() > frame.start {
                        self.stack.pop();
                    }
                    self.push(res);

                    // Restore ip
                    ip = self.curr_frame().ip.get();
                }
                OpCode::Class => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    let class = new_class(name);
                    self.push(Value::Object(class));
                }
                OpCode::Inherit => {
                    let sub_class = self.pop().unwrap_class();
                    let Ok(super_class) = self.peek(0).unwrap().try_class() else {
                        return Err(self.runtime_error("Superclass must be a class.", ip));
                    };

                    sub_class
                        .methods
                        .borrow_mut()
                        .extend(&super_class.methods.borrow());
                }
                OpCode::Method => {
                    let name = read_long_constant(chunk, &mut ip).unwrap_string();
                    self.define_method(name);
                }
            }
        }
    }

    fn concatenate(&mut self) {
        let b_obj = self.pop();

        let a_obj = self.pop();
        let a = a_obj.unwrap_string();

        let mut s = a.string.clone();
        write!(&mut s, "{b_obj}").unwrap();

        let value = Value::Object(allocate_string(
            Cow::Owned(s),
            &mut self.strings.borrow_mut(),
        ));
        self.push(value)
    }

    fn curr_frame(&self) -> &CallFrame {
        let n = self.frames.len() - 1;
        unsafe { self.frames.get_unchecked(n) }
        //self.frames.last().unwrap()
    }
}

/// Chunk read operations
fn read_byte(chunk: &Chunk, ip: &mut usize) -> u8 {
    let instruction = *unsafe { chunk.code.get_unchecked(*ip) };
    *ip += 1;
    instruction
}

fn read_short(chunk: &Chunk, ip: &mut usize) -> u16 {
    let a = read_byte(chunk, ip);
    let b = read_byte(chunk, ip);
    u16::from_le_bytes([a, b])
}

fn read_constant(chunk: &Chunk, ip: &mut usize) -> Value {
    let n = read_byte(chunk, ip);
    unsafe { chunk.constants.values.get_unchecked(n as usize).clone() }
}

fn read_long_constant(chunk: &Chunk, ip: &mut usize) -> Value {
    let n = read_short(chunk, ip);
    unsafe { chunk.constants.values.get_unchecked(n as usize).clone() }
}

/// Stack management
impl Vm {
    fn push(&mut self, value: Value) {
        self.stack.push(value);
    }

    fn pop(&mut self) -> Value {
        self.stack.pop().expect("stack underflow")
    }

    fn peek(&self, distance: usize) -> Option<&Value> {
        self.stack.get(self.stack.len() - 1 - distance)
    }

    fn replace_at_top(&mut self, distance: usize, value: Value) {
        let stack_len = self.stack.len();
        self.stack[stack_len - 1 - distance] = value;
    }

    fn check_number(&self, distance: usize) -> bool {
        self.peek(distance).is_some_and(|n| n.is_number())
    }

    fn check_string(&self, distance: usize) -> bool {
        self.peek(distance).is_some_and(|n| n.is_string())
    }
}

impl Vm {
    fn runtime_error(&mut self, error: &str, ip: usize) -> InterpretError {
        let mut err_writer = self.writers.err_writer.borrow_mut();
        writeln!(err_writer, "{}", error).unwrap();
        for (i, frame) in self.frames.iter().rev().enumerate() {
            let f = frame.closure.as_ref().function.as_ref();
            let chunk = frame.borrow_chunk();
            let instruction = if i == 0 { ip } else { frame.ip.get() - 1 };
            let line = chunk.lines.get_line(instruction);
            write!(err_writer, "[line {line}] in ").unwrap();

            if let Some(name) = &f.name {
                let name = &name.string;
                writeln!(err_writer, "{name}()").unwrap();
            } else {
                writeln!(err_writer, "script").unwrap();
            }
        }

        self.stack.clear();

        InterpretError::RuntimeError
    }
}

impl Vm {
    fn call_value(
        &mut self,
        value: Value,
        arg_count: u8,
        ip: &mut usize,
    ) -> Result<(), InterpretError> {
        let mut value_is_not_callable =
            || Err(self.runtime_error("Can only call functions and classes.", *ip));

        let Value::Object(callable) = value else {
            return value_is_not_callable();
        };

        match callable.matches() {
            ObjectMatchResult::Closure(closure) => self.call(closure, arg_count, ip),
            ObjectMatchResult::Native(native) => {
                let arity = native.arity;

                if arg_count as u16 != arity {
                    self.runtime_error(
                        &format!("Expected {arity} arguments but got {arg_count}.",),
                        *ip,
                    );
                    return Err(InterpretError::RuntimeError);
                }
                let result =
                    (native.function)(&self.stack[self.stack.len() - (arg_count as usize)..])
                        .map_err(|e| self.runtime_error(e.as_ref(), *ip))?;

                for _ in 0..arg_count + 1 {
                    self.stack.pop();
                }
                self.push(result);
                Ok(())
            }
            ObjectMatchResult::Class(class) => {
                let instance = new_instance(class.clone());

                self.replace_at_top(arg_count as usize, Value::Object(instance));

                let init_string = self.init_string.get_or_init(|| {
                    allocate_string(Cow::Borrowed("init"), &mut self.strings.borrow_mut())
                        .as_obj_string()
                });

                if let Some(initializer) = class.methods.borrow().get(init_string.clone()) {
                    self.call(initializer.clone(), arg_count, ip)?;
                } else if arg_count > 0 {
                    return Err(self.runtime_error(
                        &format!("Expected 0 arguments but got {arg_count}."),
                        *ip,
                    ));
                }
                Ok(())
            }
            ObjectMatchResult::BoundMethod(bound_method) => {
                self.replace_at_top(arg_count as usize, bound_method.receiver.clone());
                self.call(bound_method.method.clone(), arg_count, ip)
            }
            _ => value_is_not_callable(),
        }
    }

    fn call(
        &mut self,
        closure: ClosureRef,
        arg_count: u8,
        ip: &mut usize,
    ) -> Result<(), InterpretError> {
        let function = closure.function.as_ref();
        let arity = function.arity.get();
        if arg_count as u16 != arity {
            self.runtime_error(
                &format!("Expected {arity} arguments but got {arg_count}."),
                *ip,
            );
            return Err(InterpretError::RuntimeError);
        }

        let start = self.stack.len() - arg_count as usize - 1;
        let frame = CallFrame::new(closure, start);
        self.frames.push(frame);
        // reset ip to 0
        *ip = 0;
        Ok(())
    }

    fn invoke(
        &mut self,
        name: StringRef,
        arg_count: u8,
        ip: &mut usize,
    ) -> Result<(), InterpretError> {
        let Ok(instance) = self.peek(arg_count as usize).unwrap().try_instance() else {
            return Err(self.runtime_error("Only instances have methods", *ip));
        };

        if let Some(field) = instance.fields.borrow().get(name.clone()) {
            self.replace_at_top(arg_count as usize, field.clone());
            return self.call_value(field.clone(), arg_count, ip);
        }

        self.invoke_from_class(instance.class.clone(), name, arg_count, ip)
    }

    fn invoke_from_class(
        &mut self,
        class: ClassRef,
        name: StringRef,
        arg_count: u8,
        ip: &mut usize,
    ) -> Result<(), InterpretError> {
        let Some(method) = class.methods.borrow().get(name.clone()).cloned() else {
            return Err(self.runtime_error(&format!("Undefined property {}.", name.string), *ip));
        };

        self.call(method, arg_count, ip)
    }

    pub fn define_native(&mut self, name: &str, function: NativeFn, arity: u16) {
        let str = Value::Object(allocate_string(
            Cow::Borrowed(name),
            &mut self.strings.borrow_mut(),
        ));
        self.push(str);

        let native_obj = Value::Object(new_native(function, arity));
        self.push(native_obj);

        self.globals.insert(
            self.stack[0].unwrap_string(),
            Global::new(self.stack[1].clone(), false),
        );

        self.pop();
        self.pop();
    }

    fn close_upvalues(&self, last: usize) {
        let mut open_upvalues = self.open_upvalues.borrow_mut();

        while let Some(uv) = open_upvalues.deref() {
            if uv.slot < last {
                break;
            }
            uv.state
                .replace(UpvalueState::Closed(self.stack[uv.slot].clone()));
            let next = uv.next.borrow().clone();
            *open_upvalues = next;
        }
    }

    fn define_method(&mut self, name: StringRef) {
        let method = self.peek(0).unwrap().unwrap_closure();
        let class = self.peek(1).unwrap().unwrap_class();

        class.methods.borrow_mut().insert(name, method);
        self.pop();
    }

    fn bind_method(&mut self, class: ClassRef, name: StringRef) -> Result<(), InterpretError> {
        let methods = class.methods.borrow();

        let Some(method) = methods.get(name.clone()) else {
            return Err(self.runtime_error(
                &format!("Undefined property '{}'", name.string),
                self.curr_frame().ip.get(),
            ));
        };

        let bound_method = new_bound_method(self.peek(0).unwrap().clone(), method.clone());
        self.pop();
        self.push(Value::Object(bound_method));

        Ok(())
    }
}

fn capture_upvalue(index: usize, upvalues: &mut Option<UpvalueRef>) -> UpvalueRef {
    let mut prev_upvalue = None;
    let mut upvalue = upvalues.clone();

    while let Some(uv) = upvalue.clone() {
        if uv.slot <= index {
            break;
        }
        upvalue = uv.next.borrow().clone();
        prev_upvalue = Some(uv);
    }

    if let Some(uv) = upvalue.clone() {
        if uv.slot == index {
            return uv;
        }
    }

    let created_upvalue = new_upvalue(index);
    created_upvalue.next.replace(upvalue.clone());

    if let Some(prev) = prev_upvalue {
        prev.next.replace(Some(created_upvalue.clone()));
    } else {
        *upvalues = Some(created_upvalue.clone());
    }

    created_upvalue
}

#[derive(Debug)]
pub enum InterpretError {
    CompileError,
    RuntimeError,
}

impl Display for InterpretError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self, f)
    }
}

impl Error for InterpretError {}

fn is_falsey(value: &Value) -> bool {
    match value {
        Value::Nil => true,
        Value::Bool(b) => !b,
        Value::Number(_) => false,
        Value::Object(_) => false,
    }
}

#[derive(Debug)]
pub struct Global {
    value: Value,
    mutable: bool,
}

impl Global {
    pub fn new(value: Value, mutable: bool) -> Self {
        Self { value, mutable }
    }
}

struct CallFrame {
    closure: ClosureRef,
    ip: Cell<usize>,
    start: usize,
}

impl CallFrame {
    fn new(closure: ClosureRef, start: usize) -> Self {
        CallFrame {
            closure,
            ip: Cell::new(0),
            start,
        }
    }

    fn borrow_chunk(&self) -> &Chunk {
        unsafe { &*self.closure.as_ref().function.as_ref().chunk.get() }
    }
}

fn clock_native(_: &[Value]) -> Result<Value, Cow<'static, str>> {
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(n) => Ok(Value::Number(n.as_secs_f64())),
        Err(_) => Err(Cow::Borrowed("SystemTime before UNIX EPOCH!")),
    }
}

type GlobalMap = Table<Global>;
//
// impl Borrow<Rc<ObjString>> for StringKey {
//     fn borrow(&self) -> &Rc<ObjString> {
//         &self.0
//     }
// }
