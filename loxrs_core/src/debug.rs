use std::fmt::{Display, Write};

#[cfg(feature = "debug_chunk")]
use crate::chunk::{Chunk, OpCode};
use crate::object::{ObjFunction, ObjRef, ObjectMatchResult};
use crate::value::Value;

#[cfg(feature = "debug_chunk")]
use colored::Colorize;

#[cfg(feature = "debug_chunk")]
pub fn disassemble_chunk(writer: &mut dyn Write, chunk: &Chunk, name: &str) {
    writeln!(writer, "== {name} ==").unwrap();

    writeln!(writer, "-- Constants ({})", chunk.constants.values.len()).unwrap();
    for (n, v) in chunk.constants.values.iter().enumerate() {
        write!(writer, "{n}:{v:?} ").unwrap();
    }
    writeln!(writer).unwrap();

    writeln!(writer, "-- Instructions").unwrap();
    let mut offset = 0;
    while offset < chunk.code.len() {
        offset = disassemble_instruction(writer, chunk, offset);
    }
}

#[cfg(feature = "debug_chunk")]
pub fn disassemble_instruction(writer: &mut dyn Write, chunk: &Chunk, offset: usize) -> usize {
    write!(writer, "{} ", format!("{:04}", offset).yellow()).unwrap();

    let current_line = chunk.lines.get_line(offset);
    if offset > 0 && current_line == chunk.lines.get_line(offset - 1) {
        write!(writer, "   {} ", "|".bright_blue()).unwrap();
    } else {
        write!(writer, "{} ", format!("{:4}", current_line).bright_blue()).unwrap();
    }

    let raw_op_code = chunk.code[offset];
    match OpCode::try_from(raw_op_code).ok() {
        Some(instruction) => match instruction {
            OpCode::Constant => constant_instruction(writer, "Constant", chunk, offset),
            OpCode::ConstantLong => {
                constant_long_instruction(writer, "ConstantLong", chunk, offset)
            }
            OpCode::Nil => simple_instruction(writer, "Nil", offset),
            OpCode::True => simple_instruction(writer, "True", offset),
            OpCode::False => simple_instruction(writer, "False", offset),
            OpCode::Pop => simple_instruction(writer, "Pop", offset),
            OpCode::GetLocal => byte_instruction(writer, "GetLocal", chunk, offset),
            OpCode::SetLocal => byte_instruction(writer, "SetLocal", chunk, offset),
            OpCode::GetGlobal => constant_long_instruction(writer, "GetGlobal", chunk, offset),
            OpCode::DefineGlobal => {
                constant_long_instruction(writer, "DefineGlobal", chunk, offset)
            }
            OpCode::DefineGlobalVal => {
                constant_long_instruction(writer, "DefineGlobalVal", chunk, offset)
            }
            OpCode::SetGlobal => constant_long_instruction(writer, "SetGlobal", chunk, offset),
            OpCode::GetUpvalue => byte_instruction(writer, "GetUpvalue", chunk, offset),
            OpCode::SetUpvalue => byte_instruction(writer, "SetUpvalue", chunk, offset),
            OpCode::GetProperty => constant_long_instruction(writer, "GetProperty", chunk, offset),
            OpCode::SetProperty => constant_long_instruction(writer, "SetProperty", chunk, offset),
            OpCode::Equal => simple_instruction(writer, "Equal", offset),
            OpCode::Greater => simple_instruction(writer, "Greater", offset),
            OpCode::Less => simple_instruction(writer, "Less", offset),
            OpCode::Add => simple_instruction(writer, "Add", offset),
            OpCode::Subtract => simple_instruction(writer, "Subtract", offset),
            OpCode::Multiply => simple_instruction(writer, "Multiply", offset),
            OpCode::Divide => simple_instruction(writer, "Divide", offset),
            OpCode::Modulo => simple_instruction(writer, "Modulo", offset),
            OpCode::Not => simple_instruction(writer, "Not", offset),
            OpCode::Negate => simple_instruction(writer, "Negate", offset),
            OpCode::Print => simple_instruction(writer, "Print", offset),
            OpCode::JumpIfFalse => jump_instruction(writer, "JumpIfFalse", 1, chunk, offset),
            OpCode::Jump => jump_instruction(writer, "Jump", 1, chunk, offset),
            OpCode::Loop => jump_instruction(writer, "Loop", -1, chunk, offset),
            OpCode::Call => byte_instruction(writer, "Call", chunk, offset),
            OpCode::Invoke => invoke_instruction(writer, "Invoke", chunk, offset),
            OpCode::SuperInvoke => invoke_instruction(writer, "SuperInvoke", chunk, offset),
            OpCode::Closure => {
                let mut offset = offset;
                offset += 1;
                let constant = chunk.code[offset];
                offset += 1;
                write!(
                    writer,
                    "{:-16} {} ",
                    "Closure".green(),
                    format!("{constant:4}").cyan()
                )
                .unwrap();
                print_value(writer, &chunk.constants.values[constant as usize]);
                writeln!(writer).unwrap();

                let Value::Object(fun) = &chunk.constants.values[constant as usize] else {
                    panic!("function object expected");
                };
                let fun = fun.clone().as_obj_function();
                for _ in 0..fun.upvalue_count.get() {
                    let is_local = chunk.code[offset] == 1;
                    offset += 1;
                    let index = chunk.code[offset];
                    offset += 1;
                    writeln!(
                        writer,
                        "{}    |                       {} {}",
                        format!("{:04}", offset - 2).yellow(),
                        if is_local { "local" } else { "upvalue" },
                        index
                    )
                    .unwrap();
                }

                offset
            }
            OpCode::CloseUpvalue => simple_instruction(writer, "CloseUpvalue", offset),
            OpCode::Return => simple_instruction(writer, "Return", offset),
            OpCode::Class => constant_long_instruction(writer, "Class", chunk, offset),
            OpCode::Inherit => simple_instruction(writer, "Inherit", offset),
            OpCode::Method => constant_long_instruction(writer, "Method", chunk, offset),
        },
        None => {
            writeln!(writer, "Unknown opcode {raw_op_code}").unwrap();
            offset + 1
        }
    }
}

#[cfg(feature = "debug_chunk")]
fn simple_instruction(writer: &mut dyn Write, name: &str, offset: usize) -> usize {
    writeln!(writer, "{}", name.green()).unwrap();
    offset + 1
}

#[cfg(feature = "debug_chunk")]
fn byte_instruction(writer: &mut dyn Write, name: &str, chunk: &Chunk, offset: usize) -> usize {
    let slot = chunk.code[offset + 1];
    writeln!(
        writer,
        "{:-16} {}",
        name.green(),
        format!("{slot:4}").cyan()
    )
    .unwrap();
    offset + 2
}

#[cfg(feature = "debug_chunk")]
fn constant_instruction(writer: &mut dyn Write, name: &str, chunk: &Chunk, offset: usize) -> usize {
    let constant = chunk.code[offset + 1];
    write!(
        writer,
        "{:-16} {} '",
        name.green(),
        format!("{constant:4}").cyan()
    )
    .unwrap();
    print_value(writer, &chunk.constants.values[constant as usize]);
    writeln!(writer, "'").unwrap();
    offset + 2
}

#[cfg(feature = "debug_chunk")]
fn constant_long_instruction(
    writer: &mut dyn Write,
    name: &str,
    chunk: &Chunk,
    offset: usize,
) -> usize {
    let constant = u16::from_le_bytes([chunk.code[offset + 1], chunk.code[offset + 2]]);
    write!(
        writer,
        "{:-16} {} '",
        name.green(),
        format!("{constant:4}").cyan()
    )
    .unwrap();
    print_value(writer, &chunk.constants.values[constant as usize]);
    writeln!(writer, "'").unwrap();
    offset + 3
}

#[cfg(feature = "debug_chunk")]
fn jump_instruction(
    writer: &mut dyn Write,
    name: &str,
    sign: i32,
    chunk: &Chunk,
    offset: usize,
) -> usize {
    let jump = u16::from_le_bytes([chunk.code[offset + 1], chunk.code[offset + 2]]);
    write!(writer, "{:-16} ", name.green()).unwrap();
    writeln!(
        writer,
        "{}",
        format!("{offset:4} -> {}", offset as i32 + 3 + sign * jump as i32).yellow()
    )
    .unwrap();
    offset + 3
}

#[cfg(feature = "debug_chunk")]
fn invoke_instruction(writer: &mut dyn Write, name: &str, chunk: &Chunk, offset: usize) -> usize {
    let constant = u16::from_le_bytes([chunk.code[offset + 1], chunk.code[offset + 2]]);
    let arg_count = chunk.code[offset + 3];
    write!(
        writer,
        "{:-16} ({} args) {} '",
        name.green(),
        arg_count,
        format!("{constant:4}").cyan()
    )
    .unwrap();
    print_value(writer, &chunk.constants.values[constant as usize]);
    writeln!(writer, "'").unwrap();
    offset + 4
}

pub fn print_value(writer: &mut dyn Write, value: &Value) {
    write!(writer, "{}", value).unwrap();
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Bool(b) => {
                write!(f, "{b}")?;
            }
            Value::Number(n) => {
                write!(f, "{n}")?;
            }
            Value::Nil => {
                write!(f, "nil")?;
            }
            Value::Object(obj) => {
                write_obj(obj.clone(), f)?;
            }
        }
        Ok(())
    }
}

fn write_obj(obj: ObjRef, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match obj.matches() {
        ObjectMatchResult::String(s) => {
            write!(fmt, "{}", s.string)?;
        }
        ObjectMatchResult::Function(f) => {
            write_function(&f, fmt)?;
        }
        ObjectMatchResult::Native(_) => {
            write!(fmt, "<native fn>")?;
        }
        ObjectMatchResult::Closure(c) => {
            write_function(c.function.as_ref(), fmt)?;
        }
        ObjectMatchResult::Upvalue(_) => {
            write!(fmt, "upvalue")?;
        }
        ObjectMatchResult::Class(c) => {
            write!(fmt, "{}", c.name.string)?;
        }
        ObjectMatchResult::Instance(i) => {
            write!(fmt, "{} instance", i.class.name.string)?;
        }
        ObjectMatchResult::BoundMethod(b) => {
            write_function(b.method.function.as_ref(), fmt)?;
        }
    }
    Ok(())
}

fn write_function(f: &ObjFunction, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let n = &f.name;
    if let Some(name) = n {
        write!(fmt, "<fn {}>", &name.string)?;
    } else {
        write!(fmt, "<script>")?;
    }
    Ok(())
}
