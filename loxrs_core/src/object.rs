use crate::chunk::Chunk;
use crate::value::Value;
use rustc_hash::{FxBuildHasher, FxHashMap};
use std::borrow::{Borrow, Cow};
use std::cell::{Cell, RefCell, UnsafeCell};
use std::collections::hash_map::Entry;
use std::hash::{Hash, Hasher};
use std::rc::{Rc, Weak};
use weak_table::WeakHashSet;

pub trait Object {
    fn obj(&mut self) -> &mut Obj;

    fn matches(self: Rc<Self>) -> ObjectMatchResult;

    fn as_obj_string(self: Rc<Self>) -> Rc<ObjString> {
        match self.matches() {
            ObjectMatchResult::String(s) => s,
            _ => panic!("invalid cast"),
        }
    }

    fn as_obj_function(self: Rc<Self>) -> Rc<ObjFunction> {
        match self.matches() {
            ObjectMatchResult::Function(f) => f,
            _ => panic!("invalid cast"),
        }
    }

    fn as_obj_native(self: Rc<Self>) -> Rc<ObjNative> {
        match self.matches() {
            ObjectMatchResult::Native(f) => f,
            _ => panic!("invalid cast"),
        }
    }

    fn as_obj_closure(self: Rc<Self>) -> Rc<ObjClosure> {
        match self.matches() {
            ObjectMatchResult::Closure(c) => c,
            _ => panic!("invalid cast"),
        }
    }

    fn as_obj_upvalue(self: Rc<Self>) -> Rc<ObjUpvalue> {
        match self.matches() {
            ObjectMatchResult::Upvalue(uv) => uv,
            _ => panic!("invalid cast"),
        }
    }
}

pub enum ObjectMatchResult {
    String(Rc<ObjString>),
    Function(Rc<ObjFunction>),
    Native(Rc<ObjNative>),
    Closure(Rc<ObjClosure>),
    Upvalue(Rc<ObjUpvalue>),
    Class(Rc<ObjClass>),
    Instance(Rc<ObjInstance>),
    BoundMethod(Rc<ObjBoundMethod>),
}

#[derive(Default, PartialEq, Eq, Hash)]
pub struct Obj {}

#[derive(PartialEq, Eq, Hash)]
pub struct ObjString {
    obj: Obj,
    pub string: String,
}

impl ObjString {
    pub fn new(string: String) -> Self {
        Self {
            obj: Obj::default(),
            string,
        }
    }
}

impl Object for ObjString {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::String(self)
    }
}

impl Drop for ObjString {
    fn drop(&mut self) {
        #[cfg(feature = "debug_alloc")]
        {
            println!("Drop ObjString");
        }
    }
}

pub struct ObjFunction {
    obj: Obj,
    pub arity: Cell<u16>,
    pub chunk: UnsafeCell<Chunk>,
    pub name: Option<Rc<ObjString>>,
    pub upvalue_count: Cell<u8>,
}

impl Object for ObjFunction {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Function(self)
    }
}

impl ObjFunction {
    pub fn inc_arity(&self) {
        self.arity.set(self.arity.get() + 1);
    }
}

pub type NativeFn = fn(&[Value]) -> Result<Value, Cow<'static, str>>;

pub struct ObjNative {
    obj: Obj,
    pub function: NativeFn,
    pub arity: u16,
}

impl Object for ObjNative {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Native(self)
    }
}

pub struct ObjClosure {
    obj: Obj,
    pub function: Rc<ObjFunction>,
    pub upvalues: RefCell<Vec<Rc<ObjUpvalue>>>,
}

impl Object for ObjClosure {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Closure(self)
    }
}

pub struct ObjUpvalue {
    obj: Obj,
    pub next: RefCell<Option<UpvalueRef>>,
    pub slot: usize,
    pub state: RefCell<UpvalueState>,
}

#[derive(Clone)]
pub enum UpvalueState {
    Open,
    Closed(Value),
}

impl ObjUpvalue {
    pub fn new(slot: usize) -> Self {
        Self {
            obj: Obj::default(),
            next: RefCell::new(None),
            slot,
            state: RefCell::new(UpvalueState::Open),
        }
    }

    pub fn is_open(&self) -> bool {
        let b = self.state.borrow();
        match b.clone() {
            UpvalueState::Open => true,
            UpvalueState::Closed(_) => false,
        }
    }

    pub fn get_closed_value(&self) -> Value {
        let b = self.state.borrow();
        match b.clone() {
            UpvalueState::Open => panic!("upvalue is open"),
            UpvalueState::Closed(value) => value,
        }
    }
}

impl Object for ObjUpvalue {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Upvalue(self)
    }
}

pub struct ObjClass {
    obj: Obj,
    pub name: Rc<ObjString>,
    pub methods: RefCell<Table<ClosureRef>>,
}

impl Object for ObjClass {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Class(self)
    }
}

pub struct ObjInstance {
    obj: Obj,
    pub class: ClassRef,
    pub fields: RefCell<Table<Value>>,
}

impl Object for ObjInstance {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::Instance(self)
    }
}

pub struct ObjBoundMethod {
    obj: Obj,
    pub receiver: Value,
    pub method: ClosureRef,
}

impl Object for ObjBoundMethod {
    fn obj(&mut self) -> &mut Obj {
        &mut self.obj
    }

    fn matches(self: Rc<Self>) -> ObjectMatchResult {
        ObjectMatchResult::BoundMethod(self)
    }
}

pub type ObjRef = Rc<dyn Object>;

pub type StringRef = Rc<ObjString>;
pub type FunctionRef = Rc<ObjFunction>;
pub type ClosureRef = Rc<ObjClosure>;
pub type UpvalueRef = Rc<ObjUpvalue>;
pub type ClassRef = Rc<ObjClass>;

pub fn new_function(name: Option<StringRef>) -> FunctionRef {
    let f = ObjFunction {
        obj: Obj::default(),
        arity: Cell::new(0),
        chunk: UnsafeCell::new(Chunk::new()),
        upvalue_count: Cell::new(0),
        name,
    };
    Rc::new(f)
}

pub fn new_native(function: NativeFn, arity: u16) -> ObjRef {
    let f = ObjNative {
        obj: Obj::default(),
        function,
        arity,
    };
    Rc::new(f)
}

pub fn new_closure(function: FunctionRef) -> ClosureRef {
    let c = ObjClosure {
        obj: Obj::default(),
        function,
        upvalues: Default::default(),
    };
    Rc::new(c)
}

pub fn new_upvalue(slot: usize) -> UpvalueRef {
    let upvalue = ObjUpvalue::new(slot);
    Rc::new(upvalue)
}

pub fn new_class(name: StringRef) -> ClassRef {
    let c = ObjClass {
        name,
        obj: Obj::default(),
        methods: Default::default(),
    };
    Rc::new(c)
}

pub fn new_instance(class: ClassRef) -> ObjRef {
    let i = ObjInstance {
        obj: Obj::default(),
        class,
        fields: Default::default(),
    };
    Rc::new(i)
}

pub fn new_bound_method(receiver: Value, method: ClosureRef) -> ObjRef {
    let b = ObjBoundMethod {
        obj: Obj::default(),
        receiver,
        method,
    };
    Rc::new(b)
}

// struct SymbolEntry(ObjString);
//
// impl Hash for SymbolEntry {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         self.0.string.hash(state);
//     }
// }
//
// impl PartialEq for SymbolEntry {
//     fn eq(&self, other: &Self) -> bool {
//         let obj_str = self.0.as_obj_string();
//         let obj_str_b = self.0.as_obj_string();
//         obj_str.string == obj_str_b.string
//     }
// }
// impl Eq for SymbolEntry {}

impl Borrow<str> for ObjString {
    fn borrow(&self) -> &str {
        &self.string
    }
}

pub type SymbolTable = WeakHashSet<Weak<ObjString>, FxBuildHasher>;

#[derive(Clone)]
pub struct StringKey(pub StringRef);

impl PartialEq for StringKey {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }
}

impl Eq for StringKey {}

impl Hash for StringKey {
    fn hash<H: Hasher>(&self, state: &mut H) {
        std::ptr::hash(&*self.0, state);
    }
}

pub struct Table<T> {
    inner: FxHashMap<StringKey, T>,
}

impl<T> Default for Table<T> {
    fn default() -> Self {
        Self {
            inner: Default::default(),
        }
    }
}

impl<T> Table<T> {
    fn new() -> Self {
        Self::default()
    }

    pub fn get(&self, key: StringRef) -> Option<&T> {
        self.inner.get(&StringKey(key))
    }

    pub fn insert(&mut self, key: StringRef, value: T) {
        self.inner.insert(StringKey(key), value);
    }

    pub fn entry(&mut self, key: StringRef) -> Entry<StringKey, T> {
        self.inner.entry(StringKey(key))
    }
}

impl<T: Clone> Table<T> {
    pub fn extend(&mut self, other: &Table<T>) {
        self.inner
            .extend(other.inner.iter().map(|(k, v)| (k.clone(), v.clone())));
    }
}

pub fn allocate_string(s: Cow<str>, table: &mut SymbolTable) -> ObjRef {
    if let Some(entry) = table.get(s.as_ref()) {
        #[cfg(feature = "debug_alloc")]
        {
            println!("Reusing {:?}", entry.string);
        }
        entry
    } else {
        #[cfg(feature = "debug_alloc")]
        {
            println!("allocate_string {:?}", s);
        }
        let obj_str = Rc::new(ObjString::new(s.into_owned()));
        table.insert(obj_str.clone());
        obj_str
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_allocate_string() {
        // It should return the same string if it's already in the table
        let mut table = SymbolTable::default();

        let first_alloc = allocate_string(Cow::Owned("Hello".to_owned()), &mut table);
        let second_alloc = allocate_string(Cow::Owned("Hello".to_owned()), &mut table);

        asset_same_string(first_alloc, second_alloc);
    }

    #[test]
    pub fn test_allocate_string_different() {
        // It should return a different string if it's not in the table
        let mut table = SymbolTable::default();

        let first_alloc = allocate_string(Cow::Owned("Hello".to_owned()), &mut table);
        let second_alloc = allocate_string(Cow::Owned("World".to_owned()), &mut table);

        asset_different_string(first_alloc, second_alloc);
    }

    fn asset_same_string(a: ObjRef, b: ObjRef) {
        assert!(Rc::ptr_eq(&a, &b));
    }

    fn asset_different_string(a: ObjRef, b: ObjRef) {
        assert!(!Rc::ptr_eq(&a, &b));
    }

    #[test]
    fn test_string_table() {
        let mut symbol_table = SymbolTable::default();
        let mut table = Table::<usize>::new();

        let k1 = allocate_string(Cow::Borrowed("Hello"), &mut symbol_table).as_obj_string();
        table.insert(k1.clone(), 42);
        assert_eq!(table.get(k1), Some(&42));

        let k2 = allocate_string(Cow::Borrowed("Hello"), &mut symbol_table).as_obj_string();
        assert_eq!(table.get(k2), Some(&42));

        // Retrieval should be based on identity rather than value equality
        let k3 = Rc::new(ObjString::new(String::from("Hello")));
        assert_eq!(table.get(k3), None);
    }
}
