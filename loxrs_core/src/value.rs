use crate::object::{ObjBoundMethod, ObjClass, ObjClosure, ObjInstance, ObjRef};
use crate::object::{ObjFunction, ObjNative, ObjString, ObjectMatchResult};
use std::fmt::{Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::ptr;
use std::rc::Rc;

pub type RawNumber = f64;

#[derive(Clone)]
pub enum Value {
    Bool(bool),
    Number(RawNumber),
    Nil,
    Object(ObjRef),
}

impl Debug for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Bool(b) => write!(f, "Bool({})", b),
            Value::Number(n) => write!(f, "Number({})", n),
            Value::Nil => write!(f, "Nil"),
            Value::Object(ptr) => write!(f, "Object({:p})", ptr),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Value::Number(a), Value::Number(b)) => *a == *b,
            (Value::Bool(a), Value::Bool(b)) => *a == *b,
            (Value::Nil, Value::Nil) => true,
            (Value::Object(a), Value::Object(b)) => {
                match (a.clone().matches(), b.clone().matches()) {
                    (ObjectMatchResult::String(a), ObjectMatchResult::String(b)) => {
                        Rc::ptr_eq(&a, &b)
                    }
                    (ObjectMatchResult::Function(a), ObjectMatchResult::Function(b)) => {
                        Rc::ptr_eq(&a, &b)
                    }
                    _ => false,
                }
            }
            _ => false,
        }
    }
}

impl Eq for Value {}

impl Hash for Value {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            Value::Bool(b) => {
                0.hash(state);
                b.hash(state);
            }
            Value::Number(n) => {
                1.hash(state);
                n.to_bits().hash(state);
            }
            Value::Nil => {
                2.hash(state);
            }
            Value::Object(o) => {
                3.hash(state);
                // Verify correctness
                ptr::hash(o.as_ref(), state);
            }
        }
    }
}

impl Value {
    pub fn is_number(&self) -> bool {
        matches!(self, Value::Number(_))
    }

    pub fn try_number(&self) -> Result<RawNumber, ()> {
        match self {
            Value::Number(n) => Ok(*n),
            _ => Err(()),
        }
    }

    pub fn unwrap_number(&self) -> RawNumber {
        self.try_number().unwrap()
    }

    pub fn is_string(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::String(_)),
            _ => false,
        }
    }

    pub fn try_string(&self) -> Result<Rc<ObjString>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::String(s) => Ok(s),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_string(&self) -> Rc<ObjString> {
        self.try_string().unwrap()
    }

    pub fn is_function(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::Function(_)),
            _ => false,
        }
    }

    pub fn try_function(&self) -> Result<Rc<ObjFunction>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::Function(f) => Ok(f),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_function(&self) -> Rc<ObjFunction> {
        self.try_function().unwrap()
    }

    pub fn is_native(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::Native(_)),
            _ => false,
        }
    }

    pub fn try_native(&self) -> Result<Rc<ObjNative>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::Native(f) => Ok(f),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_native(&self) -> Rc<ObjNative> {
        self.try_native().unwrap()
    }

    pub fn is_closure(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::Closure(_)),
            _ => false,
        }
    }

    pub fn try_closure(&self) -> Result<Rc<ObjClosure>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::Closure(c) => Ok(c),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_closure(&self) -> Rc<ObjClosure> {
        self.try_closure().unwrap()
    }

    pub fn is_class(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::Class(_)),
            _ => false,
        }
    }

    pub fn try_class(&self) -> Result<Rc<ObjClass>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::Class(c) => Ok(c),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_class(&self) -> Rc<ObjClass> {
        self.try_class().unwrap()
    }

    pub fn is_instance(&self) -> bool {
        match self {
            Value::Object(ptr) => matches!(ptr.clone().matches(), ObjectMatchResult::Instance(_)),
            _ => false,
        }
    }

    pub fn try_instance(&self) -> Result<Rc<ObjInstance>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::Instance(i) => Ok(i),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_instance(&self) -> Rc<ObjInstance> {
        self.try_instance().unwrap()
    }

    pub fn is_bound_method(&self) -> bool {
        match self {
            Value::Object(ptr) => {
                matches!(ptr.clone().matches(), ObjectMatchResult::BoundMethod(_))
            }
            _ => false,
        }
    }

    pub fn try_bound_method(&self) -> Result<Rc<ObjBoundMethod>, ()> {
        match self {
            Value::Object(ptr) => match ptr.clone().matches() {
                ObjectMatchResult::BoundMethod(b) => Ok(b),
                _ => Err(()),
            },
            _ => Err(()),
        }
    }

    pub fn unwrap_bound_method(&self) -> Rc<ObjBoundMethod> {
        self.try_bound_method().unwrap()
    }
}

pub struct ValueArray {
    pub values: Vec<Value>,
}

impl ValueArray {
    pub fn new() -> Self {
        Self { values: vec![] }
    }

    pub fn write(&mut self, value: Value) {
        self.values.push(value);
    }
}
