extern crate core;

use clap::Parser;
use colored::control::set_override;
use rustyline::error::ReadlineError;
use rustyline::DefaultEditor;
use std::fs::File;
use std::io;
use std::io::{BufReader, Read};
use std::path::Path;
use std::process::exit;

use loxrs_core::vm::{InterpretError, Vm};
use loxrs_core::writer::Writers;

fn main() {
    let args = Args::parse();

    set_override(true);

    match args.path {
        None => {
            if args.stdin {
                run_stdin()
            } else {
                repl()
            }
        }
        Some(s) => run_file(&s),
    }
}

fn repl() {
    let writers = Writers::default();
    let mut vm = Vm::new(writers);
    let mut rl = DefaultEditor::new().expect("unable to start REPL");

    loop {
        let readline = rl.readline("> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str())
                    .expect("unable to add history");
                let _ = vm.interpret_source(&line);
            }
            Err(ReadlineError::Interrupted | ReadlineError::Eof) => {
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
}

fn run_file(path: impl AsRef<Path>) {
    let source = read_file(path).expect("unable to read source file");

    run_source(&source);
}

fn run_source(source: &str) {
    let writers = Writers::default();
    let mut vm = Vm::new(writers);
    if let Err(err) = vm.interpret_source(source) {
        match err {
            InterpretError::CompileError => exit(65),
            InterpretError::RuntimeError => exit(70),
        }
    }
}

fn read_file(path: impl AsRef<Path>) -> Result<String, io::Error> {
    let f = File::open(path)?;
    let mut b = BufReader::new(f);
    let mut contents = String::new();
    b.read_to_string(&mut contents)?;
    Ok(contents)
}

fn run_stdin() {
    let mut source = String::new();
    io::stdin().read_to_string(&mut source).unwrap();
    run_source(&source);
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Path to the script to interpret
    #[arg()]
    path: Option<String>,

    #[clap(short, long)]
    stdin: bool,
}
