use assert_cmd::assert::OutputAssertExt;
use assert_cmd::Command;

#[test]
pub fn test_expressions() {
    test_program("print 1 + 1;", "2\n");

    test_program("print 1 + 2 * 3 / 4;", "2.5\n");

    test_program("print (1 + 2) * 3 / 4;", "2.25\n");

    test_program("print \"Hello \" + \"world\";", "Hello world\n");
}

#[test]
pub fn test_literals() {
    test_program(r#"print "Hello world";"#, "Hello world\n");
    test_program("print 42;", "42\n");
    test_program("print true;", "true\n");
    test_program("print false;", "false\n");
    test_program("print nil;", "nil\n");
}

#[test]
pub fn test_global_val_cannot_be_reaffected() {
    test_program_failure(
        r#"
val a = 42;
a = 24;
print a;
    "#,
        "Cannot redefine global const \'a\'.\n[line 3] in script\n",
    )
}

#[test]
pub fn test_local_val_cannot_be_reaffected() {
    test_program_failure(
        r#"
fun main() {
    val a = 42;
    a = 24;
    print a;
}"#,
        "[line 4] Error at 24: Cannot assign to local value \'a\'\n",
    )
}

#[test]
pub fn test_interning() {
    test_program(
        r#"
{
  var a = "Hello world";
  var b = "Hello";
  var c = " world";

  print b + c == a;
}"#,
        "true\n",
    );
}

#[test]
pub fn test_variables() {
    test_program(
        "\
    var a = 1;
    var b = 2;
    {
        var c = 42;
        {
            var c = 3;
            a = a + 1; // global a is 2
            c = c + 1; // local c is 4
            print a + b + c;
        }
        print a + b + c;
    }
    ",
        "8\n46\n",
    );
}

#[test]
pub fn test_functions() {
    test_program(
        "\
        fun fib(n) {
          if (n < 2) return n;
          return fib(n - 1) + fib(n - 2);
        }

        print fib(10);
        ",
        "55\n",
    );

    test_program(
        "
        fun a() {
            print 1;
        }

        fun b() {
            fun c() { print 3; }
            a();
            print 2;
            c();
        }
        b();
        ",
        "1\n2\n3\n",
    );
}

#[test]
pub fn test_simple_closure() {
    test_program(
        r#"
fun outer() {
    var x = "outside";
    fun inner() {
        print x;
    }
    inner();
}
outer();
"#,
        "outside\n",
    );
}

#[test]
pub fn test_real_closure() {
    test_program(
        r#"
fun outer() {
    var x = "outside";
    fun inner() {
        print x;
    }
    return inner;
}
val closure = outer();
closure();
"#,
        "outside\n",
    );
}

#[test]
pub fn test_real_closure_mutation() {
    test_program(
        r#"
var globalSet;
var globalGet;

fun main() {
    var a = "initial";
    fun set() { a = "updated"; }
    fun get() { print a; }

    globalSet = set;
    globalGet = get;
}

main();
globalSet();
globalGet();
"#,
        "updated\n",
    );
}

#[test]
pub fn test_loops() {
    test_program(
        r#"
var i = 0;
var sum = 0;
while (i < 5) {
    sum = sum + i;
    i = i + 1;
}
print sum;
"#,
        "10\n",
    );

    test_program(
        r#"
var sum = 0;
for (var i = 0; i < 5; i = i + 1) {
    sum = sum + i;
}
print sum;
"#,
        "10\n",
    );

    // Test nested loops
    test_program(
        r#"
var result = 0;
for (var i = 0; i < 3; i = i + 1) {
    var j = 0;
    while (j < 2) {
        result = result + 1;
        j = j + 1;
    }
}
print result;
"#,
        "6\n",
    );
}

#[test]
pub fn test_if_else() {
    test_program(
        r#"
var a = 1;
for (var i = 0; i < 4; i = i + 1) {
    if (i % 2 == 0) {
        print "i is even";
    } else {
        print "i is not even";
    }
}
"#,
        "i is even\ni is not even\ni is even\ni is not even\n",
    );
}

#[test]
pub fn test_fibonacci() {
    test_program(
        r#"
fun fib(n) {
  if (n < 2) return n;
  return fib(n - 1) + fib(n - 2);
}
print fib(10);
"#,
        "55\n",
    );
}

#[test]
pub fn test_factorial() {
    test_program(
        r#"
fun factorial(n) {
  if (n == 0) return 1;
  return n * factorial(n - 1);
}
print factorial(5);
"#,
        "120\n",
    );
}

#[test]
pub fn test_string_concatenation() {
    test_program(
        r#"
val greeting = "Hello";
val name = " Bob";
val punctuation = "!";
print greeting + name + punctuation;

val a = "First";
val b = " ";
val c = "Second";
print a + b + c;
"#,
        "Hello Bob!\nFirst Second\n",
    );
}

#[test]
pub fn test_variable_shadowing() {
    test_program(
        r#"
val a = "outer";
{
    print a; // prints outer
    val a = "inner";
    print a; // prints inner
    {
        val a = "innermost";
        print a; // prints innermost
    }
    print a; // prints inner
}
print a; // prints outer
"#,
        "outer\ninner\ninnermost\ninner\nouter\n",
    );
}

#[test]
pub fn test_classes() {
    test_program(
        r#"
class Brioche {}

print Brioche;

val some_brioche = Brioche();
print some_brioche;

fun main() {
    class Bacon {}
    print Bacon;

    val some_bacon = Bacon();
    some_bacon.name = "Kevin";
    print some_bacon.name;
    print some_bacon.age = 42;
    print some_bacon;
}

main();
"#,
        "Brioche\nBrioche instance\nBacon\nKevin\n42\nBacon instance\n",
    );
}

#[test]
pub fn test_class_method_declaration() {
    test_program(
        r#"
class Brunch {
    bacon(a) {
        print "bacon " + a;
    }
    eggs() {}
}

val brunch = Brunch();
brunch.bacon("fried");
"#,
        "bacon fried\n",
    );
}

#[test]
pub fn test_this() {
    test_program(
        r#"
class Brunch {
    setName(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
}
fun main() {
    val bacon = Brunch();
    bacon.setName("Bacon");
    print bacon.getName();
}

main();
"#,
        "Bacon\n",
    );
}

#[test]
pub fn test_this_is_forbidden_global() {
    test_program_failure(
        r#"
        print this;
    "#,
        "[line 2] Error at ;: Cannot use \'this\' outside of a class.\n",
    );
}

#[test]
pub fn test_this_is_forbidden_inside_function() {
    test_program_failure(
        r#"
        fun notMethod() {
            print this;
        }
    "#,
        "[line 3] Error at ;: Cannot use \'this\' outside of a class.\n",
    );
}

#[test]
pub fn test_initializer() {
    test_program(
        r#"
        class Brunch {
            init(name) {
                print "init called";
                this.name = name;
            }

            getName() {
                return this.name;
            }
        }
        val brunch = Brunch("Bacon");
        print brunch.getName();
    "#,
        "init called\nBacon\n",
    );
}

#[test]
pub fn test_return_forbidden_inside_initializer() {
    test_program_failure(
        r#"
        class Brunch {
            init(name) {
                this.name = name;
                return 42;
            }

            getName() {
                return this.name;
            }
        }
        val brunch = Brunch("Bacon");
        print brunch;
    "#,
        "[line 5] Error at 42: Can\'t return a value from an initializer.\n",
    );
}

#[test]
pub fn test_instances_can_have_closure_properties() {
    test_program(
        r#"
        class Oops {
            init() {
                fun f() {
                    print "not a method";
                }
                this.field = f;
            }
        }
        
        val oops = Oops();
        oops.field();
    "#,
        "not a method\n",
    );
}

#[test]
pub fn test_inheritance() {
    test_program(
        r#"
        class Doughnut {
            cook(){
                print("Cooking a doughnut...");
            }
        }

        class Cruller < Doughnut {
            finish () {
                print("Finishing a cruller...");
            }
        }

        val cruller = Cruller();
        cruller.cook();
        cruller.finish();
    "#,
        "Cooking a doughnut...\nFinishing a cruller...\n",
    );
}

#[test]
pub fn test_inheritance_failure() {
    test_program_failure(
        r#"
        class Doughnut < Doughnut {
            cook(){
                print("Cooking a doughnut...");
            }
        }
    "#,
        "[line 2] Error at {: A class cannot inherit from itself.\n",
    );
}

#[test]
pub fn test_override() {
    test_program(
        r#"
        class Doughnut {
            cook(){
                print("Cooking a doughnut...");
            }
        }
        class Cruller < Doughnut {
            cook(){
                print("Cooking a cruller...");
            }
        }
        val cruller = Cruller();
        cruller.cook();
    "#,
        "Cooking a cruller...\n",
    );
}

#[test]
fn test_inheritance_failure_2() {
    test_program_failure(
        r#"
        val Foo = 42;
        class Doughnut < Foo {
            cook(){
            print("Cooking a doughnut...");
            }
        }
       "#,
        "Superclass must be a class.\n[line 3] in script\n",
    )
}

#[test]
pub fn test_super_invoke() {
    test_program(
        r#"
        class Doughnut {
            cook(){
                print("Dunk in the fryer");
                this.finish("sprinkles");
            }

            finish(ingredient) {
                print("Finish with " + ingredient);
            }
        }
        
        class Cruller < Doughnut {
            finish(ingredient) {
                super.finish("icing");
            }
        }
       
       val cruller = Cruller();
       cruller.cook();
    "#,
        "Dunk in the fryer\nFinish with icing\n",
    );
}

fn test_program(input: &'static str, expected: &'static str) {
    Command::cargo_bin("loxrs")
        .unwrap()
        .arg("--stdin")
        .write_stdin(input)
        .unwrap()
        .assert()
        .stdout(expected)
        .success();
}

fn test_program_failure(input: &'static str, expected_error: &'static str) {
    Command::cargo_bin("loxrs")
        .unwrap()
        .arg("--stdin")
        .write_stdin(input)
        .assert()
        .stderr(expected_error)
        .failure();
}
