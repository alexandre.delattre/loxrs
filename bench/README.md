# Useful commands

## Python bench

```bash
pyenv local 3.11
# or pyenv local 3.6
time python bench/bench.py 
```

## Rust bench

```bash
cargo build --release 
time target/release/loxrs bench/bench.lox
```

Disassembly and profiling

```bash
cargo instruments -t time --release bench/bench.lox  
cargo asm --release loxrs::vm::Vm::interpret_source --rust
```

TODO investigate UnsafeCell for CallFrame.ip field
And/or store directly current ip in local variable !

## Typical timings

```
$ time lua bench/bench.lua
102334155
lua bench/bench.lua  7.25s user 0.06s system 99% cpu 7.324 total

$ time node bench/bench.js
102334155
node bench/bench.js  0.94s user 0.01s system 99% cpu 0.963 total

$ time bun run bench/bench.js
102334155
bun run bench/bench.js  0.53s user 0.01s system 99% cpu 0.541 total

$ pyenv local 3.11 && time python bench/bench.py 
102334155
python bench/bench.py  11.42s user 0.11s system 99% cpu 11.599 total

$ pyenv local 3.6 && time python bench/bench.py  
102334155
python bench/bench.py  21.22s user 0.19s system 99% cpu 21.489 total

$ time target/release/loxrs bench/bench.lox
102334155
target/release/loxrs bench/bench.lox  14.17s user 0.09s system 98% cpu 14.461 total
```
